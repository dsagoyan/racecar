# Deep Reinforcement Learning zur Steuerung eines autonomen Roboters mittels Deep-Q-Network im diskreten Action Space #

## Agent ##
python agent.py [..arguments]

Arguments:
--settings: [settings_bgr, settings_cartpole, settings_racecar_bgr] : default=settings_racecar_bgr  
--agent: [dqn, ddpg]: default=dqn  
--act_msg_type: [Racecar, Action] : default=ActionRacecar  
--sr_msg_type: [RLStateReward, RLStateRewardRacecar] : default=RLStateRewardRacecar  
--debug: [true || false] : default=false

#### Examples ####

python agent.py --agent=dqn --act_msg_type=Action --sr_msg_type=RLStateReward --settings=settings_cartpole
python agent.py --agent=dqn --act_msg_type=Action --sr_msg_type=RLStateRewardRacecar --settings=settings_atari  

python agent.py --agent=dqn --act_msg_type=ActionRacecar --sr_msg_type=RLStateRewardRacecar  
python agent.py --agent=dqn --act_msg_type=ActionRacecar --sr_msg_type=RLStateRewardRacecar --settings=settings_bgr_20_sim  

## Environment ##

python env.py [..arguments]

Arguments:
--env: [ gazebo , openai ]
--envname: [ CartPole-v0, BreakoutDeterministic-v3 ]
--debug: [true || false] : default=false

#### Examples ####

python env.py --env=gazebo --sr_msg_type=RLStateRewardRacecar --settings=settings_bgr_20_sim  
python env.py --env=openai --envname=CartPole-v0 --sr_msg_type=RLStateReward --settings=settings_cartpole  
python env.py --env=openai --envname=BreakoutDeterministic-v3 --sr_msg_type=RLStateRewardRacecar --settings=settings_atari  
python env.py --env=openai --envname=BreakoutDeterministic-v3 --sr_msg_type=RLStateRewardRacecar --settings=settings_atari --render=true  

## How to start agent and environment nodes ##

* Run agent.py  

python agent.py --agent=dqn --act_msg_type=ActionRacecar --sr_msg_type=RLStateRewardRacecar --settings=settings_bgr_20_sim

* Run env.py  

python env.py --env=gazebo

Description:

* Environment sends first description message to Agent.
* Agent creates corresponding agent-rl and notifies with ROS Service that agent was created
* Environmnet receives the notification and sends first RLStateReward message to the agent.

## Start visualizing tool ##

1. activate tensorflow env
2. python vis.py
3. rosrun image_view image_view image:=/camera/zed/rgb/image_rect_color/vis

## Gamepad Configuration ##

$ ls -l /dev/input/js1

$ sudo chmod a+rw /dev/input/js1

$ rosparam set joy_node/dev "/dev/input/js1"
$ rosrun joy joy_node

$ rostopic echo joy

then

rosrun racecar_control joy_teleop.py

## Record Topics ##
rosbag record /vesc/ackermann_cmd_mux/output /camera/zed/rgb/image_rect_color/compressed --output-name racecar_4

## Start rx_plot (rostool) to draw rewards ##
rqt_plot /rl_agent/rl_experiment_info/episode_number /rl_agent/rl_experiment_info/episode_reward

# Results on the Youtube #
## VIDEO #1 (without obstacles) ##
[![Dueling DQN](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://youtu.be/fz4JxYr7JpY)

## VIDEO #2 (with obstacles) ##
[![Dueling DQN](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://youtu.be/2-NWt8kwV0E)

## VIDEO #3 (complex track) ##
[![Dueling DQN](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://youtu.be/V5Rw4t9ARA4)