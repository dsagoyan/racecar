#!/usr/bin/env python

'''
Date: 7.7.2017
Author: Dmitry Sagoyan
'''

from core.EnvironmentNode import EnvironmentNode

import rospy
import argparse

from utils import settings

if __name__ == '__main__':
    # Initialize the node and name it. //, anonymous = True
    rospy.init_node('racecar_env')

    parser = argparse.ArgumentParser()
    parser.add_argument('--settings', type=str, action="store", default='settings_bgr_20_sim')
    parser.add_argument('--env', type=str, action="store", default='gazebo')
    parser.add_argument('--envname', type=str, default='Gazebo')
    parser.add_argument('--sr_msg_type', action="store", default='RLStateRewardRacecar')
    parser.add_argument('--render', type=bool, action="store", default=False)
    parser.add_argument('--debug', type=bool, action="store", default=False)

    args = parser.parse_args()

    # Read settings
    settings = settings.read_settings(args.settings)

    # try:
    env_node = EnvironmentNode(args, settings)
    # except Exception as error:
    #   print(error)

    # Go to the main loop.
    rospy.spin()
