from agents.rl.core import Processor

from utils.image_processor import process_image_for_prediction
from utils.image_processor import process_image_from_pil_to_imgmsg

import numpy as np


class AtariProcessor(Processor):
    def __init__(self, model_structure=None, settings=None):
        self.model_structure = model_structure
        self.settings = settings

    def process_to_imgmsg(self, observation):
        data = process_image_from_pil_to_imgmsg(observation)
        return data

    def process_observation(self, observation):
        images_data = []

        if np.array(observation).shape[0] > 1:
            # For mini-batchs
            for obs in observation:
                for obs_el in obs:
                    images_data.append(self.apply_preprocess(obs_el)[0])

            return np.array(images_data)
        elif np.array(observation).shape[0] == 1:
            images_data.append(self.apply_preprocess(observation[0][0])[0])
            return np.asarray(images_data)

    def apply_preprocess(self, observation):
        return process_image_for_prediction(observation, self.settings)

    def process_state_batch(self, batch):
        # We could perform this processing step in `process_observation`. In this case, however,
        # we would need to store a `float16` array instead, which is 4x more memory intensive than
        # an `uint8` array. This matters if we store 1M observations.
        processed_batch = self.process_observation(batch)
        return processed_batch

        # processed_batch = batch.astype('float16') / 255.
        # return processed_batch

    def process_reward(self, reward):
        return np.clip(reward, -1., 1.)
