from agents.rl.core import Processor

from utils.image_processor import process_image_for_prediction

import numpy as np


class GazeboProcessor(Processor):
    def __init__(self, model_structure=None, settings=None):
        self.model_structure = model_structure
        self.settings = settings

        self.direct_action = (self.model_structure.env_in.num_actions -
                              self.model_structure.settings["action_space"]["steer"]["action_max_limit"]) / 2

    def process_action(self, input_action):
        action = round(
            (float(input_action) / self.direct_action) - self.model_structure.settings["action_space"]["steer"][
                "action_max_limit"], self.model_structure.settings["action_space"]["steer"]["action_decimal_point"])

        return action

    def _process_observation(self, observation):
        images_data = []

        if np.array(observation).shape[0] > 1:
            # For mini-batchs
            for obs in observation:
                for obs_el in obs:
                    images_data.append(self._apply_preprocess(obs_el)[0])

            return np.array(images_data)
        elif np.array(observation).shape[0] == 1:
            images_data.append(self._apply_preprocess(observation[0][0])[0])
            return np.asarray(images_data)

    def _apply_preprocess(self, observation):
        return process_image_for_prediction(observation, self.model_structure.settings)

    def process_state_batch(self, batch):
        # We could perform this processing step in `_process_observation`. In this case, however,
        # we would need to store a `float32` array instead, which is 4x more memory intensive than
        # an `uint8` array. This matters if we store 1M observations.
        # processed_batch = batch.astype('float16') / 255.
        # return processed_batch

        processed_batch = self._process_observation(batch)
        return processed_batch

    def process_reward(self, reward):
        return np.clip(reward, -1., 1)

        # The magnitude of the reward can be important. Since each step yields a relatively
        # high reward, we reduce the magnitude by two orders.
        # return reward
        # return round(reward / 10., 2)
