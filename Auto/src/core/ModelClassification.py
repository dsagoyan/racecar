#!/usr/bin/env python

import os
import posixpath
import keras

from keras.layers import Lambda
from keras import backend as K
from keras.layers import Dense, Flatten, Dropout, Activation
from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPooling2D, Permute

from agents.rl.keras_future import Model

from vis.utils import utils

from keras import activations

LAYER_STRUCTURE_RACECAR = 'racecar'
LAYER_STRUCTURE_CARTPOLE = 'cartpole'
LAYER_STRUCTURE_ATARI = 'atari'


class ModelClassification:
    """ModelClassification provides different CNN structure for different tasks"""

    def __init__(self, env_in=None, settings=None, vis=False):

        self.env_in = env_in
        """Environment description from EnvironmentNode"""

        self.settings = settings

        if "canny" is settings.keys():
            if settings["process_image"] == True and settings["canny"] == True:
                self.img_depth = 1
            else:
                self.img_depth = settings["img_depth"]
        else:
            self.img_depth = settings["img_depth"]

        if "action_space" in settings.keys():
            self.actions_mid = int(settings["action_space"]["steer"]["actions_num"] / 2)

        # Input Shape
        if K.image_dim_ordering() == 'th':
            # Theano-Backend
            self.input_shape = (self.settings["img_depth"], self.settings["img_height"], self.img_depth)
        else:
            # Tensorflow-Backend
            self.input_shape = (self.settings["img_height"], self.settings["img_width"], self.img_depth)

        # Load CNN model with linear activation function
        self.model = self._select_model(self.settings["layer_structure"])()

        if vis is True:
            if self.settings["dqn"]["dueling_network"]:
                # get the second last layer of the model, abandon the last layer
                layer = self.model.layers[-2]
                nb_action = self.model.output._keras_shape[-1]
                y = Dense(nb_action + 1, activation='linear')(layer.output)

                if self.settings["dqn"]["dueling_type"] == 'avg':
                    outputlayer = Lambda(
                        lambda a: K.expand_dims(a[:, 0], -1) + a[:, 1:] - K.mean(a[:, 1:], keepdims=True),
                        output_shape=(nb_action,))(y)
                elif self.settings["dqn"]["dueling_type"] == 'max':
                    outputlayer = Lambda(
                        lambda a: K.expand_dims(a[:, 0], -1) + a[:, 1:] - K.max(a[:, 1:], keepdims=True),
                        output_shape=(nb_action,))(y)
                elif self.settings["dqn"]["dueling_type"] == 'naive':
                    outputlayer = Lambda(lambda a: K.expand_dims(a[:, 0], -1) + a[:, 1:],
                                         output_shape=(nb_action,))(y)
                else:
                    assert False, "dueling_type must be one of {'avg','max','naive'}"

                self.model = Model(input=self.model.input, output=outputlayer)

            # Load DQN trained weights for visualizing
            if self.settings["visualizing"]["load_dqn_weights"]:
                self.load_weights_dqn()

    def _select_model(self, layer_structure=LAYER_STRUCTURE_RACECAR):
        # switch-case, default: _racecar
        return {
            LAYER_STRUCTURE_RACECAR: self._racecar,
            LAYER_STRUCTURE_CARTPOLE: self._cartpole,
            LAYER_STRUCTURE_ATARI: self._atari,
        }.get(layer_structure, self._racecar)

    def load_model(self):
        with K.get_session().graph.as_default():
            tmp_dir = os.path.dirname(__file__)

            filename = "trained_data/%s-best.hdf5" % (self.settings["model_name"])
            filename = os.path.join(tmp_dir, "../..", filename)
            filename = posixpath.normpath(filename)

            if os.path.isfile(filename):
                return load_model(filename)
            else:
                raise IOError

    def load_weights_dqn(self):
        with K.get_session().graph.as_default():
            filepath = posixpath.normpath(self.settings["dqn"]["weights"]["file"])

            if os.path.exists(filepath):
                print("Found DQN Weights and loaded them in the model.")
                self.model.load_weights(self.settings["dqn"]["weights"]["file"])
            else:
                print("It`s impossible to load weights because the file {} doesn`t exist".format(
                    self.settings["dqn"]["weights"]["file"]))

    def predict_on_batch(self, x):
        with K.get_session().graph.as_default():
            predicted_action = round(
                float(self.model.predict_classes(x)[0]) / self.actions_mid - self.settings["action_space"]["steer"][
                    "action_max_limit"],
                self.settings["action_space"]["steer"]["action_decimal_point"])
            return predicted_action

    # Racecar CNN
    def _racecar(self):

        if self.settings["dqn"]["load_trained_cnn_model"]:
            model_loaded = self.load_model()
            model_loaded.layers[-1].activation = activations.linear
            model_loaded = utils.apply_modifications(model_loaded)

            # Remove Dropout. Don`t need it for RL
            if self.settings["agent_node"]["remove_dropout_during_training"]:
                for k in model_loaded.layers:
                    if type(k) is keras.layers.Dropout:
                        model_loaded.layers.remove(k)
        else:
            # Create
            model = Sequential()

            # Convolution no.1
            model.add(Conv2D(name="conv1",
                             activation='relu',
                             input_shape=self.input_shape,
                             padding='valid',
                             filters=8,
                             kernel_size=(5, 5)
                             ))
            model.add(MaxPooling2D(name="maxpool1", pool_size=(2, 2), strides=(1, 1)))

            # Convolution no.2
            model.add(Conv2D(name="conv2",
                             activation='relu',
                             padding='valid',
                             filters=12,
                             kernel_size=(5, 5)
                             ))
            model.add(MaxPooling2D(name="maxpool2", pool_size=(2, 2), strides=(1, 1)))

            # Convolution no.3
            model.add(Conv2D(name="conv3",
                             activation='relu',
                             padding='valid',
                             filters=16,
                             kernel_size=(5, 5),
                             ))
            model.add(MaxPooling2D(name="maxpool3", pool_size=(2, 2), strides=(2, 2)))

            # Convolution no.4
            model.add(Conv2D(name="conv4",
                             activation='relu',
                             padding='valid',
                             filters=24,
                             kernel_size=(3, 3)
                             ))

            # Convolution no.5
            model.add(Conv2D(name="conv5",
                             activation='relu',
                             padding='valid',
                             filters=24,
                             kernel_size=(3, 3)))

            model.add(Flatten())

            # Fully connected no.1
            model.add(Dense(units=256, activation='relu', name='fl1'))

            # Fully connected no.2
            model.add(Dense(units=100, activation='relu', name='fl2'))

            # Fully connected no.3
            model.add(Dense(units=50, activation='relu', name='fl3'))

            model.add(Dense(units=int(self.settings["action_space"]["steer"]["actions_num_output"] + 1),
                            activation='softmax'))

            # compile
            model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
            model.layers[-1].activation = activations.linear
            model_loaded = utils.apply_modifications(model)

        return model_loaded

    # Cartpole CNN
    def _cartpole(self):
        # Create model
        model = Sequential()
        model.add(Flatten(input_shape=(1,) + (4,)))
        model.add(Dense(16))
        model.add(Activation('relu'))
        model.add(Dense(16))
        model.add(Activation('relu'))
        model.add(Dense(16))
        model.add(Activation('relu'))
        model.add(Dense(int(self.env_in.num_actions)))
        model.add(Activation('linear'))
        return model

    # Atari Games CNN
    def _atari(self):
        INPUT_SHAPE = (84, 84)
        WINDOW_LENGTH = 4
        input_shape = (WINDOW_LENGTH,) + INPUT_SHAPE

        model = Sequential()

        if K.image_dim_ordering() == 'tf':
            # (width, height, channels)
            model.add(Permute((2, 3, 1), input_shape=input_shape))
        elif K.image_dim_ordering() == 'th':
            # (channels, width, height)
            model.add(Permute((1, 2, 3), input_shape=input_shape))
        else:
            raise RuntimeError('Unknown image_dim_ordering.')

        model.add(Conv2D(32, (8, 8), strides=(4, 4)))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (4, 4), strides=(2, 2)))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (3, 3), strides=(1, 1)))
        model.add(Activation('relu'))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dense(int(self.env_in.num_actions)))
        model.add(Activation('linear'))

        return model
