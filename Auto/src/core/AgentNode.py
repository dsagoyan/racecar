#!/usr/bin/env python

'''
Date: 23.04.2017
Author: Dmitry Sagoyan
'''

import numpy as np
import rospy

from keras import backend as K
import tensorflow as tf

from collections import deque

from agents.DQN import DQN

from racecar_msgs.msg import RLEnvDescription
from racecar_msgs.msg import RLExperimentInfo

from racecar_msgs.msg import RLStateReward
from racecar_msgs.msg import RLStateRewardRacecar

from racecar_msgs.msg import Action
from racecar_msgs.msg import ActionRacecar

from ModelClassification import ModelClassification

from std_srvs.srv import Empty, EmptyResponse

from utils import file_writer
from utils import vislog


class AgentNode:
    """AgentNode represents different RL-Agents (DQN,DDPG) and also is responsible to communicate with RL-Environment.    

    Arguments:
    args -- contains different arguments from ArgumentParser
    args.settings -- Setting file [ex. settings_bgr_20_sim]
    args.agent -- Implementation of Reinforcement Learning Agent [ex. dqn]
    args.act_msg_type -- ROS-Message for Action [ex. ActionRacecar]
    args.sr_msg_type -- ROS-Message for StateReward [ex. RLStateRewardRacecar]
    args.debug -- Activate debug mode
    """

    def __init__(self, args, settings):
        print 'Agent Node'

        self.settings = settings
        """Configuraiton settings"""

        self.arguments = args
        """Application arugments"""

        self.setup_comparing()

        self.setup_node()

        self.setup_publishers()

        self.setup_subscribers()

    def setup_comparing(self):
        """Setup comparing properties"""

        self.comparing_episodes_history = None
        self.comparing_status = False

        if "comparing" in self.settings["dqn"]:
            self.comparing_status = self.settings["dqn"]["comparing"]["status"]

            if self.comparing_status:
                self.comparing_usecases_json = []
                self.comparing_usecases_idx = 0
                self.comparing_usecases = []
                self.comparing_episodes = self.settings["dqn"]["comparing"]["episodes"]

                for case in self.settings["dqn"]["comparing"]["usecases"]:
                    self.comparing_usecases.append(self.settings["dqn"]["comparing"]["usecases"][case])

                # Collect sum of the rewards of the episodes
                self.comparing_episodes_history = deque(maxlen=self.settings["dqn"]["comparing"]["episodes"])

    def setup_node(self):
        """Setup default node properties"""
        self.process_sr_stop = True
        self.episode_history = None

        # Episode history capacity
        if self.settings["dqn"]["history_condition"] and \
                self.settings["dqn"]["history_condition"]["qty_of_episode_history"]:
            self.episode_history = deque(maxlen=self.settings["dqn"]["history_condition"]["qty_of_episode_history"])
            self.qty_of_episode_history = self.settings["dqn"]["history_condition"]["qty_of_episode_history"]

        self.reset_agent_node()

    def reset_agent_node(self):
        """Reset agent node with default properties which should be reset after each new episode"""

        self.start_episode_new = True

        self.weights_saving = False

        if self.episode_history is not None:
            self.episode_history.clear()

        if self.comparing_episodes_history is not None:
            self.comparing_episodes_history.clear()

    def setup_publishers(self):
        """Setup ROS publishers"""
        self.pub_rl_action = rospy.Publisher("/rl_agent/rl_action",
                                             self.get_action_message_type(True),
                                             queue_size=1,
                                             latch=False)

        # After end of the episode, send all data to the rl-environment.
        self.pub_rl_exp_info = rospy.Publisher("/rl_agent/rl_experiment_info",
                                               RLExperimentInfo,
                                               queue_size=1,
                                               latch=False)

    def setup_subscribers(self):
        """Setup ROS subscribers"""
        rospy.Subscriber("/rl_env/rl_env_description",
                         RLEnvDescription,
                         self.process_env_description,
                         queue_size=1)

        rospy.Subscriber("/rl_env/rl_state_reward",
                         self.get_sr_message_type(True),
                         self.process_state_reward,
                         queue_size=1)

    def get_action_message_type(self, retClass=True):
        """Return either object of arguments.act_msg_type or the class description
        
        Arguments: 
        retClass -- if True, then returns class description. if False, then class instance
        """
        if retClass:
            return eval(self.arguments.act_msg_type)
        else:
            return eval(self.arguments.act_msg_type)()

    def get_sr_message_type(self, retClass=True):
        """Return either object of arguments.sr_message_type or the class description

        Arguments:
        retClass -- if True, then returns class description. if False, then class instance
        """
        if retClass:
            return eval(self.arguments.sr_msg_type)
        else:
            return eval(self.arguments.sr_msg_type)()

    def save_model_weights(self):
        """Save model free parameters (weights)"""
        self.weights_saving = True

        self.rl_agent.model_agent.reset_step_counter()

        self.rl_agent.model_agent.save_weights(
            self.settings["dqn"]["weights"]["file"],
            self.settings["dqn"]["weights"]["overwrite"])

        self.weights_saving = False

        print("\n***********The RL model weights have been saved.***********\n")

    def process_state_reward(self, seed_in):
        """Subscription callback for /rl_env/rl_state_reward ROS-Topic.
        
        Arguments:
        seed_in: see StateReward ROS-Message
        """
        if seed_in.firstState:
            self.process_sr_stop = False

        # If process_sr_stop is True, then trained model has achieved
        # the result or there is a job to create a new agent
        if not self.process_sr_stop:

            # Start a new episode
            if self.start_episode_new == True:

                # Set to false to know that episode has just started
                self.start_episode_new = False

                self.rl_agent.model_agent.start_episode()

                # Pull an action
                self.generate_and_fire_action(seed_in.state)
            else:
                # Get new state, reward and terminal by proceed action
                self.rl_agent.model_agent.store_rollout(seed_in.reward,
                                                        seed_in.terminal,
                                                        self.settings["dqn"]["verbose"]["show_measure_after_backward"])

                # If terminal is true, no action to send back, but calculate reward sum
                if seed_in.terminal == True:

                    # Add total_rewards of the episode into the history
                    self.episode_history.append(self.rl_agent.model_agent.episode_reward)

                    # Get mean of histories
                    mean_rewards = np.mean(self.episode_history)

                    if self.settings["agent_node"]["log_episode_summary"]:
                        print(
                            "\nEpisode: {} "
                            "\nFinished after {} timesteps"
                            "\nReward for this episode: {}"
                            "\nAverage reward for last {} episodes: {}\n".format(
                                self.rl_agent.model_agent.episode,
                                self.rl_agent.model_agent.episode_step,
                                self.rl_agent.model_agent.episode_reward,
                                self.qty_of_episode_history,
                                mean_rewards))

                    # Finishing conditions
                    if self.settings["dqn"]["history_condition"]["finishing"]:
                        if mean_rewards >= self.settings["dqn"]["history_condition"]["mean_rewards"] and \
                                        len(self.episode_history) == self.settings["dqn"]["history_condition"][
                                    "qty_of_episode_history"]:

                            print("\nSolved after {} episodes".format(self.rl_agent.model_agent.episode))

                            self.process_sr_stop = True
                            self.rl_agent.model_agent.stop_train()

                            # Save weights if the finish condition = true
                            if self.settings["dqn"]["weights"]["save"]:
                                self.rl_agent.model_agent.save_weights(
                                    self.settings["dqn"]["weights"]["file"],
                                    self.settings["dqn"]["weights"]["overwrite"])

                    # Save weights
                    if not self.comparing_status and \
                            not self.weights_saving and \
                            self.settings["dqn"]["training"] and \
                            self.settings["dqn"]["weights"]["save"] and \
                            self.settings["dqn"]["weights"]["save_after_steps"] and \
                                    self.settings["dqn"]["weights"][
                                        "save_after_steps"] < self.rl_agent.model_agent.step_counter:
                        self.save_model_weights()

                    # Temporary save before publishing
                    episode = self.rl_agent.model_agent.episode
                    episode_reward = self.rl_agent.model_agent.episode_reward

                    # If comparing is true, and history not none, then collect episode_reward
                    if self.comparing_status and self.comparing_episodes_history is not None:
                        self.comparing_episodes_history.append(episode_reward)

                    # Backward, then end episode
                    self.rl_agent.model_agent.end_episode(seed_in.state)

                    if self.comparing_status and (self.rl_agent.model_agent.episode % self.comparing_episodes == 0):
                        # Stop processing to have a time for new RL-Agent creation
                        self.process_sr_stop = True

                        # Write comparing log
                        comparing_usecase_log_json = file_writer.write_comparing_log(
                            self.comparing_usecases[self.comparing_usecases_idx],
                            self.comparing_episodes_history)

                        # Save in array json file to use it for visualizing after the comparing function finishes its work
                        self.comparing_usecases_json.append(comparing_usecase_log_json)

                        self.reset_agent_node()

                        self.comparing_usecases_idx += 1

                        if self.comparing_usecases_idx == len(self.comparing_usecases):
                            print("Comparing is over. Please check logs.")
                            self.process_sr_stop = True
                            self.rl_agent.model_agent.stop_train()

                            vislog.visualize_comparing_logs(self.comparing_usecases_json,
                                                            self.settings["dqn"]["comparing"][
                                                                'visualize_each_episodes_mean'])
                        else:
                            self._create_rl_agent(self.comparing_usecases[self.comparing_usecases_idx])
                    else:
                        self.start_episode_new = True

                        # Publish Exp
                        rl_exp_info = RLExperimentInfo()
                        rl_exp_info.episode_number = episode
                        rl_exp_info.episode_reward = episode_reward
                        # /rl_agent/rl_experiment_info
                        self.pub_rl_exp_info.publish(rl_exp_info)
                else:
                    # Pull an action from new state
                    self.generate_and_fire_action(seed_in.state)

    def generate_and_fire_action(self, state):
        """Generate an action by observation and send it to the racecar_environment"""

        # Note: under recent_observation and recent_action are saved
        # this state and generated action, they are used in backward to
        # save in memory

        # Get an action for state
        action = self.rl_agent.model_agent.forward(state)

        if self.settings["agent_node"]["log_generated_actions"]:
            print("\nGenerated Steering Action: {}".format(action))

        # Create action obj, fill in and return it
        a = self.get_action_message_type(False)

        if (self.arguments.act_msg_type.lower() == 'actionracecar'):
            a.steer_action = action
            a.speed_action = 1
            a.episode_step = self.rl_agent.model_agent.episode_step
            a.reward_multiplier = self.rl_agent.model_agent.reward_multiplier
        elif (self.arguments.act_msg_type.lower() == 'action'):
            a.action = action
        else:
            raise None

        self.rl_agent.model_agent.callbacks.on_action_begin(action)

        # Publish an action
        self.pub_rl_action.publish(a)

        self.rl_agent.model_agent.callbacks.on_action_end(action)

    def process_env_description(self, env_in):
        """Subscription callback for /rl_env/rl_env_description ROS-Topic. Called, when the environment sends the env-description to the agent """
        print "\nProcess Env Description Received: \n {}".format(env_in)

        self.env_in = env_in

        if self.comparing_status:
            self._create_rl_agent(self.comparing_usecases[self.comparing_usecases_idx])
        else:
            self._create_rl_agent()

    def srv_agent_created(self):
        """Notify racecar_env about creation of the rl-agent using ROS-Service"""
        try:
            rospy.wait_for_service("/rl_agent/rl_agent_created")
            rl_agent_created_srv = rospy.ServiceProxy("/rl_agent/rl_agent_created", Empty())
            rl_agent_created_srv()
        except:
            rospy.logwarn("ROS Service (srv_agent_created) is not available")

    def _create_rl_agent(self, comparing_usecases=None):
        """RL-Agent is initialized"""

        # Clear session and destroy old graphs
        K.clear_session()

        if K.backend() == 'tensorflow':
            tf.reset_default_graph()

        if self.arguments.agent.upper() == "DQN":
            # Load basic CNN model for bootstraping RL
            model_classification = ModelClassification(
                env_in=self.env_in,
                settings=self.settings)

            self.rl_agent = eval(self.arguments.agent.upper())(self, model_classification,
                                                               comparing_usecases=comparing_usecases)
        else:
            raise NotImplementedError

        # Notify Environment about created Agent
        self.srv_agent_created()
