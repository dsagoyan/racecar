#!/usr/bin/env python
import os
import posixpath

from keras import backend as K
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Flatten
from keras.models import Sequential
from keras.models import load_model

LAYER_STRUCTURE_RACECAR = 'racecar'


class SupervisedModel(object):
    def __init__(self, settings=None, training=False):
        self.training = training
        self.settings = settings

        if "canny" is settings.keys():
            if settings["process_image"] == True and settings["canny"] == True:
                # image processing outputs a greyscale (mainly black and white)
                self.img_depth = 1
            else:
                self.img_depth = settings["img_depth"]
        else:
            self.img_depth = settings["img_depth"]

        self.actions_mid = int(settings["action_space"]["steer"]["actions_num"] / 2)

        # Input Shape
        if K.image_dim_ordering() == 'th':
            # Theano-Backend
            self.input_shape = (self.img_depth, self.settings["img_height"], self.settings["img_width"])
        else:
            # Tensorflow-Backend
            self.input_shape = (self.settings["img_height"], self.settings["img_width"], self.img_depth)

        if training:
            self.model = self._select_model(settings["layer_structure"])()
        else:
            self.model = self.load_model()

    def _select_model(self, layer_structure=LAYER_STRUCTURE_RACECAR):
        # switch-case, default: _racecar
        return {
            LAYER_STRUCTURE_RACECAR: self._racecar,
        }.get(layer_structure, self._racecar)

    def load_model(self):
        with K.get_session().graph.as_default():
            tmp_dir = os.path.dirname(__file__)
            filename = "trained_data/%s-best.hdf5" % (self.settings["model_name"],)
            filename = os.path.join(tmp_dir, "../..", filename)
            filename = posixpath.normpath(filename)
            if os.path.isfile(filename):
                return load_model(filename)
            else:
                raise IOError

    def predict_on_batch(self, x):
        with K.get_session().graph.as_default():
            if self.settings["type"] == "classification":
                return round(
                    float(self.model.predict_classes(x)[0]) / self.actions_mid - self.settings["action_space"]["steer"][
                        "action_max_limit"],
                    self.settings["action_space"]["steer"]["action_decimal_point"])
            else:
                return self.model.predict(x, verbose=0)

    def make_stateful_predictor(self):
        return lambda x: self.predict_on_batch(x)

    # RACECAR's CNN architecture
    def _racecar(self):
        # Load model and Weights
        if self.settings["model_load"]:
            return self.load_model()

        # Create
        model = Sequential()

        # Convolution no.1
        model.add(Conv2D(name="conv1",
                         activation='relu',
                         input_shape=self.input_shape,
                         padding='valid',
                         filters=8,
                         kernel_size=(5, 5)
                         ))
        model.add(MaxPooling2D(name="maxpool1", pool_size=(2, 2), strides=(1, 1)))

        # Convolution no.2
        model.add(Conv2D(name="conv2",
                         activation='relu',
                         padding='valid',
                         filters=12,
                         kernel_size=(5, 5)
                         ))
        model.add(MaxPooling2D(name="maxpool2", pool_size=(2, 2), strides=(1, 1)))

        # Convolution no.3
        model.add(Conv2D(name="conv3",
                         activation='relu',
                         padding='valid',
                         filters=16,
                         kernel_size=(5, 5),
                         ))
        model.add(MaxPooling2D(name="maxpool3", pool_size=(2, 2), strides=(2, 2)))

        # Convolution no.4
        model.add(Conv2D(name="conv4",
                         activation='relu',
                         padding='valid',
                         filters=24,
                         kernel_size=(3, 3)
                         ))

        # Convolution no.5
        model.add(Conv2D(name="conv5",
                         activation='relu',
                         padding='valid',
                         filters=24,
                         kernel_size=(3, 3)))

        model.add(Flatten())

        if self.settings["type"] == "classification":

            if self.settings["action_space"]["steer"]["actions_num_output"] == 20:
                # Fully connected no.1
                model.add(Dense(units=256, activation='relu', name='fl1'))
                model.add(Dropout(0.2))

                # Fully connected no.2
                model.add(Dense(units=100, activation='relu', name='fl2'))
                model.add(Dropout(0.2))

                # Fully connected no.3
                model.add(Dense(units=50, activation='relu', name='fl3'))
                model.add(Dropout(0.2))

            elif self.settings["action_space"]["steer"]["actions_num_output"] == 200:
                # Fully connected no.1
                model.add(Dense(units=384, activation='relu', name='fl1'))
                model.add(Dropout(0.2))

                # Fully connected no.2
                model.add(Dense(units=200, activation='relu', name='fl2'))
                model.add(Dropout(0.2))

                # Fully connected no.3
                # model.add(Dense(units=150, activation='relu', name='fl3'))
                # model.add(Dropout(0.2))

                # Fully connected no.4
                # model.add(Dense(output_dim=312, activation='relu', name='fl4'))
                # model.add(Dropout(0.2))
            elif self.settings["action_space"]["steer"]["actions_num_output"] == 100:
                # Fully connected no.1
                model.add(Dense(units=512, activation='relu', name='fl1'))
                model.add(Dropout(0.3))

                # Fully connected no.2
                model.add(Dense(units=300, activation='relu', name='fl2'))
                model.add(Dropout(0.3))

                # Fully connected no.3
                model.add(Dense(units=200, activation='relu', name='fl3'))
                model.add(Dropout(0.3))

                # Fully connected no.4
                # model.add(Dense(units=200, activation='relu', name='fl4'))
                # model.add(Dropout(0.2))

            model.add(Dense(units=int(self.settings["action_space"]["steer"]["actions_num_output"] + 1),
                            activation='softmax'))

            # compile
            model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        else:
            # Fully connected no.1
            model.add(Dense(units=256, activation='relu'))
            model.add(Dropout(0.2))

            # Fully connected no.2
            model.add(Dense(units=100, activation='relu'))
            model.add(Dropout(0.2))

            # Fully connected no.3
            model.add(Dense(units=50, activation='relu'))
            model.add(Dropout(0.2))

            # Fully connected no.4
            model.add(Dense(units=10, activation='relu'))
            model.add(Dropout(0.2))

            # Fully connected no.5
            model.add(Dense(units=1, activation='tanh'))

            # compile
            model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])

        return model
