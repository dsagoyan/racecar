#!/usr/bin/env python
'''
The MIT License (MIT)

Copyright (c) 2017 Patrick Baumann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

from ackermann_msgs.msg import AckermannDriveStamped

SPEED_ACCELERATE_MAX = 1.0
SPEED_REVERSE_MAX = -1.0

SPEED_STOP = 0.0
SPEED_REVERSE = -0.8
SPEED_STEP_SIZE = 0.2
SPEED_FIXED = 2

STEERING_ANGLE_MAX = 1.0
STEERING_ANGLE_RESET = 0
STEERING_ANGLE_DEAD = 0.25
AXES_STEP_SIZE = 0.1
AXES_TRIGGER_RESET = 1  # is 1 since Button has value 1 for not being triggered


class DriveControl:
    def __init__(self):
        self.speed = SPEED_STOP
        self.accelerator = AXES_TRIGGER_RESET
        self.brake = AXES_TRIGGER_RESET
        self.steering_angle = STEERING_ANGLE_RESET

    def update_driving(self, _steering=0, _accelerator=1, _brake=1, _speed=None):
        if _speed is None:
            self._update_steering(_steering)
            self._update_speed(_accelerator, _brake)

        a_d_s = AckermannDriveStamped()

        a_d_s.drive.steering_angle = self.steering_angle
        a_d_s.drive.steering_angle_velocity = 0
        a_d_s.drive.acceleration = 0

        if _speed is None:
            a_d_s.drive.speed = self.speed
        else:
            a_d_s.drive.speed = _speed

        a_d_s.drive.jerk = 0

        return a_d_s

    def _update_speed(self, _accelerator, _brake):
        if _accelerator == -1.0 and _brake == -1.0:
            self.reverse()
            return

        if _accelerator == -1.0:
            # self._accelerate()
            self._drive_fixed_speed()
            return

        if _brake == -1.0:
            self._brake()
            return

    # TODO
    def _update_speed_adjusted_to_trigger(self, _accelerator, _brake):
        if _accelerator == -1.0 and _brake == -1.0:
            self.reverse()
            return

        if (self.accelerator - AXES_STEP_SIZE) > _accelerator:
            self.accelerator -= AXES_STEP_SIZE
            self._accelerate()
            return
        else:
            self.accelerator = _accelerator

        if (self.brake - AXES_STEP_SIZE) > _brake:
            self.accelerator -= AXES_STEP_SIZE
            self._brake()
            return
        else:
            self.brake = _brake

    def _update_steering(self, _steering):
        # if _steering < 0:
        #     # negative
        #     if _steering < -STEERING_ANGLE_MAX:
        #         _steering = -STEERING_ANGLE_MAX
        #     elif _steering > -0.2:
        #         _steering = 0
        # else:
        #     # positive
        #     if _steering > STEERING_ANGLE_MAX:
        #         _steering = STEERING_ANGLE_MAX
        #     elif _steering < 0.2:
        #         _steering = 0

        self.steering_angle = _steering

    def _accelerate(self):
        if self.speed < SPEED_ACCELERATE_MAX:
            self.speed += SPEED_STEP_SIZE

    def _drive_fixed_speed(self):
        self.speed = SPEED_FIXED

    def _brake(self):
        if self.speed > SPEED_STOP:
            self.speed -= SPEED_STEP_SIZE

    def reverse(self):
        if self.speed < SPEED_REVERSE_MAX:
            self.speed = SPEED_REVERSE
