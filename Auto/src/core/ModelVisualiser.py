#!/usr/bin/env python

'''
Date: 7.7.2017
Author: Dmitry Sagoyan
'''

import pylab as plt
import numpy as np
import random

from matplotlib import pyplot as plt

from keras import activations

from utils.image_processor import process_image_for_prediction
from utils.image_processor import cv2_to_imgmsg

from vis.visualization import visualize_saliency
from vis.visualization import visualize_activation
from vis.utils import utils

from sensor_msgs.msg import Image
from racecar_msgs.msg import CompressedImage

import rospy

from keras import backend as K


class ModelVisualiser():
    """Visualizing ConvNet

    There are two possibilities:
    a. Live. It's possible to subscribe to node and draw the data by using image_view node.
    b. Offline. Give the model and just use the data from the files.
    """

    def __init__(self, supervised_model, converter):
        self.supervised_model = supervised_model
        """Model Classification"""

        if self.supervised_model.settings["visualizing"]["public"]:
            rospy.init_node('visualize_cam_data', anonymous=True)

            self.layer_idx = self.supervised_model.settings["visualizing"]["layer_idx"]

            if not self.supervised_model.settings["visualizing"]["class_idx"]:
                self.class_idx = self.supervised_model.settings["visualizing"]["class_idx"]
            else:
                self.class_idx = None

            # The Softmax is already changed to Linear in modelClassification
            # self.swap_softmax_to_linear(self.layer_idx)

            self.pub = rospy.Publisher(self.supervised_model.settings["visualizing"]["publish"],
                                       Image,
                                       queue_size=self.supervised_model.settings["visualizing"]["queue_size"])

            rospy.Subscriber(self.supervised_model.settings["visualizing"]["subscribe"],
                             CompressedImage,
                             self._visualize_callback,
                             queue_size=self.supervised_model.settings["visualizing"]["queue_size"])

            rospy.spin()
        else:

            # Take each n-th data from files
            n_data_from_data = self.supervised_model.settings["visualizing"]["n_data_from_data"]

            # Empty list will use all subfolders
            data_sub_folders = self.supervised_model.settings["supervised_learning"]["converter"]["data_sub_folders"]

            # Get data from data folder
            (x_train, y_train_sa, y_train_speed), (x_test, y_test_sa, y_test_speed) = converter.return_data(
                settings=self.supervised_model.settings,
                folders=data_sub_folders,
                color_scale=supervised_model.settings["color_scale"],
                height=supervised_model.settings["img_height"],
                width=supervised_model.settings["img_width"],
                process_image=supervised_model.settings["process_image"],
                n_data=n_data_from_data)

            # Visualize Images
            # self.vis_imgs(x_test)

            # Visualize Image
            # self.vis_img(x_test[0])

            possible_classes = self.get_possible_classes(y_test_sa)

            print("Possible Classes:\n{}".format(possible_classes))
            print("Enter Classes (exp: 1, 10): ")
            input = raw_input()
            classes = input.split(',')
            classes = [int(x) for x in classes]

            # Visualize Activation by Layer Index
            # m_vis.vis_activation(x_test,
            #                     y_test_sa,
            #                     class_idxs=classes,
            #                     layer_idx=-1,
            #                     modifiers=[None, 'guided', 'relu'])

            # Visualize Saliency by Layer Index
            self.vis_saliency(x_test,
                              y_test_sa,
                              class_idxs=classes,
                              layer_idx=-1,
                              modifiers=[None, 'guided', 'relu'])

            # Visualize Saliency by Layer Name
            # m_vis.vis_saliency(x_test,
            #                   y_test_sa,
            #                   class_idxs=classes,
            #                   layer_name="fl3",
            #                   modifiers=[None, 'guided', 'relu'])

    def _visualize_callback(self, data):
        """Process and return CV Image only"""

        data = process_image_for_prediction(data, self.supervised_model.settings, onlyCV2=True)

        with K.get_session().graph.as_default():
            data = self.generate_saliency(self.layer_idx, data, class_idx=self.class_idx)
            imgmsg = cv2_to_imgmsg(data, "bgr8")

            self.pub.publish(imgmsg)

    def get_config(self):
        print(self.supervised_model.model.get_config())

    def get_count_params(self):
        print(self.supervised_model.model.count_params())

    def get_possible_classes(self, y_test):
        possible_classes = []
        for class_idx in range(self.supervised_model.settings["action_space"]["steer"]["actions_num"] + 1):
            indices = np.where(y_test[:, class_idx] == 1.)[0]
            if len(indices) > 0:
                possible_classes.append(class_idx)
                # print("Class {} -> {}".format(class_idx, len(indices)))
        return possible_classes

    def vis_img(self, img, show=True):
        """Visualize image"""
        plt.rcParams['figure.figsize'] = (18, 6)
        plt.imshow(img[..., 0])

        if show == True:
            plt.show()

    def vis_imgs(self, x_test):
        """Visualize all images in the list"""
        for img in x_test:
            self.vis_img(img)

    def vis_activation(self, x_test, y_test, class_idxs=[0], layer_idx=-1, layer_name=None, modifiers=[None]):
        """ Visualize Activation Layers
        Generates an attention heatmap over the seed_input for
        maximizing filter_indices output in the given  layer_idx.

        https: // raghakot.github.io / keras - vis / vis.visualization /  # visualize_activation

        :param x_test: x dataset
        :param y_test: y dataset
        :param class_idxs: filter indices within the layer to be maximized. If None, all filters are visualized. (Default value = None)
        :param layer_idx: The layer index within model.layers whose filters needs to be visualized.
        :param layer_name: The layer name within model.layers whose filters needs to be visualized.
        :param modifiers: None, 'guided', 'relu'
        :return:
        """
        self._visualize(visualize_activation,
                        x_test,
                        y_test,
                        class_idxs,
                        layer_idx,
                        layer_name,
                        modifiers)

    def vis_saliency(self, x_test, y_test, class_idxs=[0], layer_idx=-1, layer_name=None, modifiers=[None]):
        """Visualize Saliency Layers
        Generates the model input that maximizes the output of all
        filter_indices in the given layer_idx

        https://raghakot.github.io/keras-vis/vis.visualization/#visualize_saliency

        :param x_test: x dataset
        :param y_test: y dataset
        :param class_idxs: filter indices within the layer to be maximized. If None, all filters are visualized. (Default value = None)
        :param layer_idx: The layer index within model.layers whose filters needs to be visualized.
        :param layer_name: The layer name within model.layers whose filters needs to be visualized.
        :param modifiers: None, 'guided', 'relu'
        :return:
        """
        self._visualize(visualize_saliency,
                        x_test,
                        y_test,
                        class_idxs,
                        layer_idx,
                        layer_name,
                        modifiers)

    def _visualize(self, vis_func, x_test, y_test, class_idxs=[0], layer_idx=-1, layer_name=None, modifiers=[None]):
        """

        :param vis_func: reference to vis_activation or vis_saliency
        :param x_test: x dataset
        :param y_test: y dataset
        :param class_idxs: filter indices within the layer to be maximized. If None, all filters are visualized. (Default value = None)
        :param layer_idx: The layer index within model.layers whose filters needs to be visualized.
        :param layer_name: The layer name within model.layers whose filters needs to be visualized.
        :param modifiers: None, 'guided', 'relu'
        :return:
        """
        if layer_name is not None:
            layer_idx = utils.find_layer_idx(self.supervised_model.model, layer_name)

        # self.swap_softmax_to_linear(layer_idx)

        fig, axes = plt.subplots(len(class_idxs), len(modifiers) + 1)

        class_counter = 0

        if len(class_idxs) > 1:
            for axrow in axes:
                self._create_subplot(vis_func,
                                     axrow,
                                     x_test,
                                     y_test,
                                     class_idxs[class_counter],
                                     layer_idx,
                                     modifiers)
                class_counter = class_counter + 1
        else:
            self._create_subplot(vis_func,
                                 axes,
                                 x_test,
                                 y_test,
                                 class_idxs[class_counter],
                                 layer_idx,
                                 modifiers)

        plt.show()

    def swap_softmax_to_linear(self, layer_idx):
        # Swap softmax with linear. Should be changed from Softmax to get real values
        self.supervised_model.model.layers[layer_idx].activation = activations.linear
        self.supervised_model.model = utils.apply_modifications(self.supervised_model.model)

    def _create_subplot(self, vis_func, axrow, x_test, y_test, class_idx, layer_idx, modifiers):

        # Get qty of samples by class
        indices = np.where(y_test[:, class_idx] == 1.)[0]

        if len(indices) == 0:
            raise Exception("There is no image in x_test dataset for class '{}'".format(class_idx))

        # Get First or Random Image

        # idx = indices[0]
        # or random
        idx = random.randint(0, len(indices) - 1)

        img = x_test[indices[idx]]

        axrow[0].set_title("Class: {}".format(class_idx))
        axrow[0].imshow(img[..., 0])

        for col, modifier in enumerate(modifiers):
            heatmap = self._generate_heatmap(vis_func,
                                             layer_idx,
                                             img,
                                             class_idx=class_idx,
                                             modifier=modifier)
            if modifier is None:
                modifier = 'vanilla'

            axrow[col + 1].set_title("Modifier: {}".format(modifier))
            axrow[col + 1].imshow(heatmap)

    def generate_saliency(self, layer_idx, img, class_idx=None, modifier=None):
        return self._generate_heatmap(visualize_saliency, layer_idx, img, class_idx, modifier)

    def generate_activation(self, layer_idx, img, class_idx=None, modifier=None):
        return self._generate_heatmap(visualize_activation, layer_idx, img, class_idx, modifier)

    def _generate_heatmap(self, vis_func, layer_idx, img, class_idx=None, modifier=None):
        heatmap = vis_func(self.supervised_model.model,
                           layer_idx,
                           class_idx,
                           img,
                           modifier)
        # filter_indices=class_idx,
        # seed_input=img,
        # backprop_modifier=modifier)
        return heatmap
