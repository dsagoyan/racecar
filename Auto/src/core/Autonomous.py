#!/usr/bin/env python
'''
The MIT License (MIT)

Copyright (c) 2017 Patrick Baumann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import CompressedImage
from DriveControl import DriveControl

from utils.image_processor import process_image_for_prediction

import numpy as np
import rospy

from keras import backend as K


class Autonomous:
    drive_control = DriveControl()

    def __init__(self, model):
        self.pub = None
        self.model = model

    def _callback(self, data, prediction):
        image_data = process_image_for_prediction(data, self.model.settings)

        with K.get_session().graph.as_default():
            steering_command = np.squeeze(prediction(image_data))
            acc_command = -1.0
            brake_command = 1.0

        # Disables self driving and gives possibility to control with the gamepad
        if self.model.settings["supervised_learning"]["manual_control"] == False:
            a_d_s = self.drive_control.update_driving(_steering=steering_command,
                                                      _accelerator=acc_command,
                                                      _brake=brake_command)
            self.pub.publish(a_d_s)

        rospy.loginfo(steering_command)

    def start(self, wait_for_input=True, queue_size=20):
        rospy.init_node('read_cam_data', anonymous=True)

        self.pub = rospy.Publisher('vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped,
                                   queue_size=queue_size)

        if wait_for_input:
            raw_input("Press Enter to start")

        rospy.Subscriber("camera/zed/rgb/image_rect_color/compressed", CompressedImage, self._callback,
                         self.model.make_stateful_predictor())

        rospy.spin()
