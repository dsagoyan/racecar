#!/usr/bin/env python

'''
Date: 22.04.2017
Author: Dmitry Sagoyan
'''

import rospy

from racecar_msgs.msg import ActionRacecar
from racecar_msgs.msg import RLStateRewardRacecar

from racecar_msgs.msg import Action
from racecar_msgs.msg import RLStateReward

from racecar_msgs.msg import RLEnvDescription
from racecar_msgs.msg import RLExperimentInfo

from env.Gazebo import Gazebo
from env.Openai import Openai

from DriveControl import DriveControl

from std_srvs.srv import Empty, EmptyResponse


class EnvironmentNode:
    """EnvironmentNode represents different RL-Environments (Gazebo,OpenAI) and also is responsible to communicate with RL-Agent.

    Arguments:
    args -- contains different arguments from ArgumentParser
    args.settings -- Setting file [ex. settings_bgr_20_sim]
    args.env -- Implementation of Reinforcement Learning Environment [ex. gazebo, openai]
    args.envname -- Name of the Environment [Gazebo, CartPole-v0, BreakoutDeterministic-v3]
    args.sr_msg_type -- ROS-Message for StateReward [ex. RLStateRewardRacecar]
    args.render -- Animate OpenAI environment
    args.debug -- Activate debug mode

    Libraries:
    1. Gazebo: offers the ability to accurately and efficiently simulate populations of robots in complex indoor and outdoor environments.
    2. OpenAI: is a non-profit AI research company, discovering and enacting the path to safe artificial general intelligence.
    """

    def __init__(self, args, settings):
        self.settings = settings
        """Configuraiton settings"""

        self.arguments = args
        """Application arugments"""

        self.env_obj = self.create_env()
        """Environment could be different (Gazebo, OpenAI and so on)"""

        if self.env_obj is None:
            raise Exception("The environment {} can not be created.".format(self.arguments.env.lower()))

        self.setup_comparing()

        self.setup_publishers()
        self.setup_subscribers()
        self.setup_services()

        # Send environment description to rl_agent and right after that first state
        self.send_rl_env_desc()

    def setup_comparing(self):
        """Setup comparing properties"""

        self.comparing_status = False

        if "comparing" in self.settings["dqn"]:
            self.comparing_status = self.settings["dqn"]["comparing"]["status"]

            if self.comparing_status:
                self.comparing_usecases_idx = 0
                self.comparing_usecases = []
                self.comparing_episodes = self.settings["dqn"]["comparing"]["episodes"]

                for case in self.settings["dqn"]["comparing"]["usecases"]:
                    self.comparing_usecases.append(self.settings["dqn"]["comparing"]["usecases"][case])

    def setup_services(self):
        rospy.Service('/rl_agent/rl_agent_created', Empty, self.send_rl_env_first_state)

    def setup_publishers(self):
        """Setup ROS publishers"""
        self.pub_env_desc = rospy.Publisher("/rl_env/rl_env_description", RLEnvDescription, queue_size=1, latch=True)
        self.pub_env_sr = rospy.Publisher("/rl_env/rl_state_reward", self.env_obj.get_sr_message_type(True),
                                          queue_size=1)

    def setup_subscribers(self):
        """Setup ROS subscribers"""
        rospy.Subscriber("/rl_agent/rl_action",
                         self.env_obj.get_action_message_type(True),
                         self.env_obj.process_action,
                         queue_size=1)

        rospy.Subscriber("/rl_agent/rl_experiment_info",
                         RLExperimentInfo,
                         self.process_episode_info,
                         queue_size=1)

    def create_env(self):
        """RL-Environment is initialized"""

        if self.arguments.env.lower() == 'gazebo':
            return Gazebo(self, DriveControl())
        elif self.arguments.env.lower() == 'openai':
            return Openai(self)
        else:
            raise None

    def send_rl_env_desc(self):
        """Send environment description to rl_agent and right after that first state"""

        # Prepare description
        desc = RLEnvDescription()
        desc.title = self.env_obj.get_env_name()
        desc.num_actions = self.env_obj.get_num_actions()
        desc.episodic = self.env_obj.is_episodic()

        # Publish environment description
        self.pub_env_desc.publish(desc)

        print "Environment Description has been sent: \n {}\n".format(desc)

    def send_rl_env_first_state(self, res):
        """Send first (state,reward,terminal)"""

        # Here the env shoud be reset and unpaussed to get image,reward and terminal and send them
        self.env_obj.reset()

        if self.comparing_status:
            # Set a new reward function
            self.env_obj.set_reward_function(self.comparing_usecases[self.comparing_usecases_idx])

            # Go to the next usecase
            self.comparing_usecases_idx += 1

        self.prepare_and_send_state()

        print "First State Message was sent\n"

    def process_episode_info(self, info_in):
        """Process end-of-episode reward info. Mostly to start new episode"""

        if self.arguments.debug:
            # Start new episode if terminal
            print "\nEpisode {} terminated with reward: {}. Start new episode ".format(info_in.episode_number,
                                                                                       info_in.episode_reward)
        # reset environment before getting state
        self.env_obj.reset()

        self.prepare_and_send_state()

    def prepare_and_send_state(self):
        """Used to send a first state after an each episode-start"""

        # First of all, unpause-> get properties -> pause the physics (if in settings)
        observation, reward, terminal = self.env_obj.get_observation_reward_terminal(firstState=True)

        # Prepare first state to send
        sr = self.env_obj.get_sr_message_type(False)
        sr.terminal = terminal
        sr.reward = reward
        sr.state = observation
        sr.firstState = True

        # /rl_env/rl_state_reward -> RLAgent -> process_state_reward
        self.pub_env_sr.publish(sr)

        self.env_obj.first_state_sent = True
