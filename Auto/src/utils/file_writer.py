#!/usr/bin/env python
import os
import posixpath
import json

from time import localtime, strftime
import matplotlib.pyplot as plt

from keras.utils.vis_utils import plot_model

import numpy as np


def write_nn_structure(model=None):
    tmp_dir = os.path.dirname(__file__)
    model.model = model.load_model()
    filepath = "graphs/%s" % (model.settings["model_name"],)
    filepath = os.path.join(tmp_dir, "../..", filepath)
    filepath = posixpath.normpath(filepath)
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    filepath = "%s/%s_nn_structure.png" % (filepath, model.settings["model_name"],)
    filepath = posixpath.normpath(filepath)
    plot_model(model.model, to_file=filepath, show_shapes=True, show_layer_names=True)


def write_comparing_log(usecase, comparing_episodes_history):
    tmp_dir = os.path.dirname(__file__)
    filepath = "logs/comparing"
    filepath = os.path.join(tmp_dir, "../..", filepath)
    filepath = posixpath.normpath(filepath)

    if not os.path.exists(filepath):
        os.makedirs(filepath)

    time = strftime("%Y-%m-%d %H:%M:%S", localtime())

    # filepath_txt = "{}/{}_{}_{}.log".format(filepath, time, usecase["learning_rate"], usecase["reward_function"])
    # filepath_txt = posixpath.normpath(filepath_txt)

    filepath_json = "{}/{}_{}_{}.json".format(filepath, time, usecase["learning_rate"], usecase["reward_function"])
    filepath_json = posixpath.normpath(filepath_json)

    episodes_qty = len(comparing_episodes_history)
    episode_rewards_sum = np.sum(comparing_episodes_history)
    episode_rewards_mean = np.mean(comparing_episodes_history)

    ########## Text Logs
    '''
    log_data = "Episodes:{}\n" \
               "Learning Rate: {}\n" \
               "Reward_Function: {}\n" \
               "Sum (Rewards) of Episodes: {}\n" \
               "Mean (Rewards) of Episodes: {}".format(
        episodes_qty,
        usecase["learning_rate"],
        usecase["reward_function"],
        episode_rewards_sum,
        episode_rewards_mean
    )

    # Writing txt log
    with open(filepath_txt, "a") as log_text:
        log_text.write("{}\n".format(log_data))
    '''

    ########## JSON Logs
    data_json = {'episodes': {}, 'info': {}, 'vis': {}}
    data_json["info"]["episodes_qty"] = episodes_qty
    data_json["info"]["learning_rate"] = usecase["learning_rate"]
    data_json["info"]["reward_function"] = usecase["reward_function"]
    data_json["info"]["sum"] = episode_rewards_sum
    data_json["info"]["mean"] = episode_rewards_mean
    data_json["vis"]["logline"] = usecase["logline"]

    for z in range(len(comparing_episodes_history)):
        data_json["episodes"][str((z + 1))] = comparing_episodes_history[z]

    # Writing json log
    with open(filepath_json, 'w') as outfile:
        json.dump(data_json, outfile, sort_keys=True)

    return filepath_json


def write_log_text(model_name=None, history=None):
    tmp_dir = os.path.dirname(__file__)
    filepath = "logs/%s" % (model_name,)
    filepath = os.path.join(tmp_dir, "../..", filepath)
    filepath = posixpath.normpath(filepath)

    if not os.path.exists(filepath):
        os.makedirs(filepath)

    time = strftime("%Y-%m-%d %H:%M:%S", localtime())
    filepath = "%s/%s.log" % (filepath, time,)
    filepath = posixpath.normpath(filepath)

    for nb_epoch in history.epoch:
        log_data = "epoch: %s | loss: %s - acc: %s - val_loss: %s - val_acc: %s" % (
            str(nb_epoch + 1),
            str(history.history['loss'][nb_epoch]),
            str(history.history['acc'][nb_epoch]),
            str(history.history['val_loss'][nb_epoch]),
            str(history.history['val_acc'][nb_epoch])
        )
        with open(filepath, "a") as log_text:
            log_text.write("{}\n".format(log_data))


def write_log_graphs(model_name=None, history=None):
    tmp_dir = os.path.dirname(__file__)
    filepath = "logs/%s" % (model_name,)
    filepath = os.path.join(tmp_dir, "../..", filepath)
    filepath = posixpath.normpath(filepath)
    if not os.path.exists(filepath):
        os.makedirs(filepath)

    # path for accuracy log
    time = strftime("%Y-%m-%d %H:%M:%S", localtime())
    acc = "%s/%s_acc.png" % (filepath, time,)
    acc = posixpath.normpath(acc)

    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    acc_title = "%s accuracy" % (model_name,)
    plt.title(acc_title)
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(acc)

    # path for history log
    loss = "%s/%s_loss.png" % (filepath, time,)
    loss = posixpath.normpath(loss)

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    loss_title = "%s loss" % (model_name,)
    plt.title(loss_title)
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(loss)
