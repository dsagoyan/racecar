#!/usr/bin/env python

'''
Date: 7.7.2017
Author: Dmitry Sagoyan
'''

import json
import os
import posixpath
import collections


def read_settings(settings_filename):
    """Read settings json file from settings folder and return json object.
    
    Arguments:
    settings_filename -- file name
    """

    assert settings_filename is not None

    tmp_dir = os.path.dirname(__file__)
    filename = "../settings/%s.json" % (settings_filename,)
    filename = os.path.join(tmp_dir, filename)
    filename = posixpath.normpath(filename)

    if os.path.isfile(filename):
        with open(filename) as data_file:
            settings = json.load(data_file, object_pairs_hook=collections.OrderedDict)
    else:
        raise IOError

    return settings
