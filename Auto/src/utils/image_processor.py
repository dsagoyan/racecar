#!/usr/bin/env python

from cv2 import line, resize, Canny, cvtColor, COLOR_BGR2GRAY, INTER_CUBIC
from cv_bridge import CvBridge
from PIL import Image

import numpy as np
import cv_bridge
import cv2

cv_bridge = CvBridge()


def process_image(image=None, settings=None):
    # Draw line of reference for AI
    line(image, (int(image.shape[1] / 2), image.shape[0]), (int(image.shape[1] / 2), int(image.shape[0] / 4) * 3),
         (255, 255, 255), 1)

    if settings is not None and settings["canny"] == True:
        image = Canny(image, 100, 110)
        image = np.reshape(image, (image.shape[0], image.shape[1], 1))

    return image


def process_image_from_pil_to_imgmsg(data):
    return cv_bridge.cv2_to_compressed_imgmsg(cv2.cvtColor(np.asarray(Image.fromarray(data)), cv2.COLOR_RGB2BGR))


def process_image_for_prediction(data, settings, onlyCV2=False):
    cv_image = cv_bridge.compressed_imgmsg_to_cv2(data, settings["color_scale"].encode('latin_1'))

    if settings["resize_camera_data"]:
        cv_image = resize(cv_image, (settings["img_width"], settings["img_height"]), interpolation=INTER_CUBIC)

    if not onlyCV2:
        image_data = np.array([cv_image]).astype('uint8')

    if settings["color_scale"] == "mono8":
        image_data = np.reshape(image_data, (1, image_data.shape[1], image_data.shape[2], 1))

    if settings["process_image"]:
        image_data = np.squeeze(image_data)
        image_data = process_image(image_data, settings)

        if settings["canny"]:
            img_depth = 1
        else:
            img_depth = settings["img_depth"]

        image_data = np.reshape(image_data, (1, image_data.shape[0], image_data.shape[1], img_depth))

    if not onlyCV2:
        image_data = image_data.astype('float16')
        image_data /= 255
        return image_data
    else:
        return cv_image


def cv2_to_imgmsg(data, enc):
    return cv_bridge.cv2_to_imgmsg(data, enc)


def img_to_grey(image):
    return cvtColor(image, COLOR_BGR2GRAY)
