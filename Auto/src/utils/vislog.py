#!/usr/bin/env python

'''
Date: 7.7.2017
Author: Dmitry Sagoyan
'''

from __future__ import print_function

import matplotlib.pyplot as plt
import json
import numpy as np


def visualize_comparing_logs(json_files, visualize_each_episodes_mean=100):
    """Visualizing comparing logs

    Arguments:
    json_files -- json files represent comparing log files and its python array
    """

    plt.xlabel('Episodes')
    plt.ylabel('Rewards')

    for json_file in json_files:
        x = []
        y = []
        episodes_mean = []
        with open(json_file, 'r') as f:
            data = json.load(f)

            json_data_episodes_keys = sorted(data['episodes'].keys(), key=lambda x: int(x))

            for episode_key in json_data_episodes_keys:
                episodes_mean.append(data['episodes'][episode_key])

                if int(episode_key) % visualize_each_episodes_mean == 0:
                    x.append(episode_key.encode('latin_1'))  # 20,40,60...
                    y.append(np.mean(episodes_mean))  # mean of episode_rewards between min and min+20

                    episodes_mean = []

            line_info = "LR: {}; RF: {}; VisType: {}; Diff(Max,Min): {}".format(
                data['info']['learning_rate'],
                data['info']['reward_function'],
                data['vis']['logline'],
                str(np.max(y) - np.min(y)))

            plt.plot(x, y, data['vis']['logline'], label=line_info)
            plt.legend()

    plt.show()


if __name__ == '__main__':
    # Could be used directly
    visualize_comparing_logs(
        ['/home/dim/Soft/racecar/Auto/logs/comparing/2017-08-10 18:17:53_1e-05_reward_function_2.json',
         '/home/dim/Soft/racecar/Auto/logs/comparing/2017-08-10 15:15:24_0.0001_reward_function_2.json',
         '/home/dim/Soft/racecar/Auto/logs/comparing/2017-08-10 10:51:22_0.00025_reward_function_2.json',
         ])
