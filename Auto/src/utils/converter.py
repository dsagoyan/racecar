#!/usr/bin/env python

'''
The MIT License (MIT)

Copyright (c) 2017 Patrick Baumann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import json
import os
import posixpath

import numpy as np
from cv2 import INTER_CUBIC
from cv2 import imread
from cv2 import imwrite
from cv2 import resize
from rosbag import Bag
from cv_bridge import CvBridge
from rospy import loginfo
from rospy.rostime import Time

from utils.image_processor import process_image as process_img
from utils.image_processor import img_to_grey

SPLIT_BY_FRACTION = 7  # 1/7 used as test data, rest for training purpose
REDUCE_INPUT_BY_SECOND = 5
GREY_SCALE = "mono8"

from keras.utils import np_utils


def convert_rosbags_to_data(settings, bags=list(), folder_dir="test", color_scale="bgr8", n_data=10):
    """
    :param bags: use list of bags, empty list uses whole folder rosbags
    :param folder_dir:
    :param color_scale:
    :param n_data: take only each n-th data
    :return:
    """
    tmp_dir = os.path.dirname(__file__)
    tmp_folder_dir = "training_data/data/%s" % (folder_dir,)
    tmp_folder_dir = os.path.join(tmp_dir, "../..", tmp_folder_dir)
    tmp_folder_dir = posixpath.normpath(tmp_folder_dir)

    if not os.path.exists(tmp_folder_dir):
        os.makedirs(tmp_folder_dir)

    if not bags:
        tmp_folder = os.path.join(tmp_dir, "../..", "training_data/rosbags")
        tmp_folder = posixpath.normpath(tmp_folder)
        for tmp_file in os.listdir(tmp_folder):
            if tmp_file.endswith(".bag"):
                tmp = os.path.join(tmp_folder, tmp_file)
                tmp = posixpath.normpath(tmp)
                bags.append(tmp)
    else:
        tmp_bags = []
        for bag_name in bags:
            tmp_bag = "training_data/rosbags/%s.bag" % (bag_name,)
            tmp_bag = os.path.join(tmp_dir, "../..", tmp_bag)
            tmp_bag = posixpath.normpath(tmp_bag)
            tmp_bags.append(tmp_bag)
        bags = tmp_bags

    bags_folder = "training_data/rosbags"
    bags_folder = os.path.join(tmp_dir, "../..", bags_folder, "")
    bags_folder = posixpath.normpath(bags_folder)
    bags_folder = os.path.join(bags_folder, "")

    cv_bridge = CvBridge()

    data_json = {'y_train': {}, 'y_test': {}}

    for tmp_bags in bags:
        print("\nExtracting data from bag: {}\n".format(tmp_bags))
        json_key_prefix = tmp_bags.replace(bags_folder, "")
        json_key_prefix = str(json_key_prefix.replace(".bag", ""))

        with Bag(tmp_bags, 'r') as bag:
            # cut the first and last seconds, since sometimes the amount of msgs from each topic is not in sync
            start_time = Time(int(bag.get_start_time() + REDUCE_INPUT_BY_SECOND))
            end_time = Time(int(bag.get_end_time() - REDUCE_INPUT_BY_SECOND))

            tmp_time = end_time - start_time
            tmp_time = Time(int(tmp_time.to_sec() / SPLIT_BY_FRACTION))
            start_end_time = Time((end_time - tmp_time).to_sec())

            skip_data = n_data
            for topic, msg, t in bag.read_messages(start_time=start_time, end_time=start_end_time):
                if skip_data % n_data == 0:
                    if topic == settings["supervised_learning"]["converter"]["camera_topic"].encode('ascii', 'ignore'):

                        # Read appropriate action
                        for topic_2, msg_2, t_2 in bag.read_messages(
                                topics=settings["supervised_learning"]["converter"]["steering_topic"].encode('ascii',
                                                                                                             'ignore'),
                                start_time=t):
                            timestamp = "%s_%s" % (json_key_prefix, t,)
                            data_json['y_train'][timestamp] = _get_data(msg_2)
                            break

                        cv_image = cv_bridge.compressed_imgmsg_to_cv2(msg, color_scale.encode('latin_1'))
                        filename = "%s/%s_%s.jpg" % (tmp_folder_dir, json_key_prefix, t,)
                        filename = posixpath.normpath(filename)
                        imwrite(filename, cv_image)
                skip_data += 1

            skip_data = n_data
            for topic, msg, t in bag.read_messages(start_time=start_end_time, end_time=end_time):
                if skip_data % n_data == 0:
                    if topic == settings["supervised_learning"]["converter"]["camera_topic"].encode('ascii', 'ignore'):
                        for topic_2, msg_2, t_2 in bag.read_messages(
                                topics=settings["supervised_learning"]["converter"]["steering_topic"].encode('ascii',
                                                                                                             'ignore'),
                                start_time=t):
                            timestamp = "%s_%s" % (json_key_prefix, t,)
                            data_json['y_test'][timestamp] = _get_data(msg_2)
                            break

                        cv_image = cv_bridge.compressed_imgmsg_to_cv2(msg, color_scale.encode('latin_1'))
                        filename = "%s/%s_%s.jpg" % (tmp_folder_dir, json_key_prefix, t,)
                        filename = posixpath.normpath(filename)
                        imwrite(filename, cv_image)
                skip_data += 1

    filename = "%s/%s.json" % (tmp_folder_dir, folder_dir,)
    with open(filename, 'w') as outfile:
        json.dump(data_json, outfile)

    loginfo('Converting successful.')


def return_rosbags(settings, bags=list(), width=64, height=48, color_scale="bgr8", process_image=False, n_data=10):
    """
    :param settings: array of settings
    :param bags: use list of bags, empty list uses whole folder rosbags
    :param color_scale:
    :param width:
    :param height:
    :param process_image:
    :param n_data: take only each n-th data:
    :return:
    """

    tmp_dir = os.path.dirname(__file__)

    if not bags:
        tmp_folder = os.path.join(tmp_dir, "../..", "training_data/rosbags")
        tmp_folder = posixpath.normpath(tmp_folder)
        for tmp_file in os.listdir(tmp_folder):
            if tmp_file.endswith(".bag"):
                tmp = os.path.join(tmp_folder, tmp_file)
                tmp = posixpath.normpath(tmp)
                bags.append(tmp)
    else:
        tmp_bags = []
        for bag_name in bags:
            tmp_bag = "training_data/rosbags/%s.bag" % (bag_name,)
            tmp_bag = os.path.join(tmp_dir, "../..", tmp_bag)
            tmp_bag = posixpath.normpath(tmp_bag)
            tmp_bags.append(tmp_bag)
        bags = tmp_bags

    cv_bridge = CvBridge()

    x_train = []
    y_train_speed = []
    y_train_sa = []

    x_test = []
    y_test_speed = []
    y_test_sa = []

    print('Bags found: {}'.format(len(bags)))

    for bag_name in bags:
        with Bag(bag_name, 'r') as bag:
            # cut the first and last seconds, since sometimes the amount of msgs from each topic is not in sync
            start_time = Time(int(bag.get_start_time() + REDUCE_INPUT_BY_SECOND))
            end_time = Time(int(bag.get_end_time() - REDUCE_INPUT_BY_SECOND))
            tmp_time = end_time - start_time
            tmp_time = Time(int(tmp_time.to_sec() / SPLIT_BY_FRACTION))
            start_end_time = Time((end_time - tmp_time).to_sec())

            skip_data = n_data
            for topic, msg, t in bag.read_messages(start_time=start_time, end_time=start_end_time):
                if skip_data % n_data == 0:
                    if topic == settings["supervised_learning"]["converter"]["camera_topic"]:
                        for topic_2, msg_2, t_2 in bag.read_messages(
                                topics=settings["supervised_learning"]["converter"]["steering_topic"].encode('ascii',
                                                                                                             'ignore'),
                                start_time=t):

                            if settings['type'] == "classification":
                                y_train_sa.append(_get_classif_action(msg_2.drive.steering_angle, "steer", settings))
                                y_train_speed.append(_get_classif_action(msg_2.drive.speed, "speed", settings))
                            else:
                                y_train_sa.append(msg_2.drive.steering_angle)
                                y_train_speed.append(msg_2.drive.speed)
                            break

                        cv_image = cv_bridge.compressed_imgmsg_to_cv2(msg, color_scale.encode('latin_1'))
                        image_data = resize(cv_image, (width, height), interpolation=INTER_CUBIC)
                        image_data = np.array(image_data).astype('uint8')

                        if color_scale == GREY_SCALE:
                            image_data = np.reshape(image_data, (image_data.shape[0], image_data.shape[1], 1))

                        if process_image:
                            image_data = process_img(image_data, settings)

                        x_train.append(image_data)
                skip_data += 1

            skip_data = n_data
            for topic, msg, t in bag.read_messages(start_time=start_end_time, end_time=end_time):
                if skip_data % n_data == 0:
                    if topic == settings["supervised_learning"]["converter"]["camera_topic"]:
                        for topic_2, msg_2, t_2 in bag.read_messages(
                                topics=settings["supervised_learning"]["converter"]["steering_topic"].encode('ascii',
                                                                                                             'ignore'),
                                start_time=t):

                            if settings['type'] == "classification":
                                y_test_sa.append(_get_classif_action(msg_2.drive.steering_angle, "steer", settings))
                                y_test_speed.append(_get_classif_action(msg_2.drive.speed, "speed", settings))
                            else:
                                y_test_sa.append(msg_2.drive.steering_angle)
                                y_test_speed.append(msg_2.drive.speed)
                            break

                        cv_image = cv_bridge.compressed_imgmsg_to_cv2(msg, color_scale.encode('latin_1'))
                        image_data = resize(cv_image, (width, height), interpolation=INTER_CUBIC)
                        image_data = np.array(image_data).astype('uint8')

                        if color_scale == GREY_SCALE:
                            image_data = np.reshape(image_data, (image_data.shape[0], image_data.shape[1], 1))

                        if process_image:
                            image_data = process_img(image_data, settings)

                        x_test.append(image_data)
                skip_data += 1

    x_train = np.array(x_train)
    y_train_speed = np.array(y_train_speed)
    y_train_sa = np.array(y_train_sa)

    x_test = np.array(x_test)
    y_test_speed = np.array(y_test_speed)
    y_test_sa = np.array(y_test_sa)

    if settings['type'] == "classification":
        y_train_sa = np_utils.to_categorical(y_train_sa, settings["action_space"]["steer"]["actions_num_output"] + 1)
        y_train_speed = np_utils.to_categorical(y_train_speed,
                                                settings["action_space"]["speed"]["actions_num_output"] + 1)
        y_test_sa = np_utils.to_categorical(y_test_sa, settings["action_space"]["steer"]["actions_num_output"] + 1)
        y_test_speed = np_utils.to_categorical(y_test_speed,
                                               settings["action_space"]["speed"]["actions_num_output"] + 1)

    if x_train.shape[0] != y_train_sa.shape[0]:
        loginfo('Error (Steering Angle): x_train and y_train have different shapes')
        loginfo(x_train.shape[0], y_train_sa.shape[0])
        raise IOError

    if x_train.shape[0] != y_train_speed.shape[0]:
        loginfo('Error (Speed): x_train and y_train have different shapes')
        loginfo(x_train.shape[0], y_train_speed.shape[0])
        raise IOError

    if x_test.shape[0] != y_test_sa.shape[0]:
        loginfo('Error (Steering Angle): x_test and y_test have different shapes')
        loginfo(x_test.shape[0], y_test_sa.shape[0])
        raise IOError

    if x_test.shape[0] != y_test_speed.shape[0]:
        loginfo('Error (Speed): x_test and y_test have different shapes')
        loginfo(x_test.shape[0], y_test_speed.shape[0])
        raise IOError

    print('All Bags read')

    return (x_train, y_train_sa, y_train_speed), (x_test, y_test_sa, y_test_speed)


def return_data(settings, folders=list(), width=64, height=48, color_scale="bgr8", process_image=False, n_data=5):
    """
    :param settings: array of settings
    :param folders: use list of folders, empty list uses whole folder training_data/data
    :param width:
    :param height:
    :param color_scale:
    :param process_image:
    :param n_data:
    :return:
    """

    tmp_dir = os.path.dirname(__file__)
    tmp_folder = os.path.join(tmp_dir, "../..", "training_data/data")

    if not folders:
        dirs = os.listdir(tmp_folder)
    else:
        dirs = folders

    x_train = []
    y_train_speed = []
    y_train_sa = []

    x_test = []
    y_test_speed = []
    y_test_sa = []

    for tmp_dir in dirs:
        tmp = "%s/%s/%s.json" % (tmp_folder, tmp_dir, tmp_dir,)
        tmp = posixpath.normpath(tmp)
        with open(tmp) as data_file:
            data = json.load(data_file)

        skip_data = n_data
        for key in data['y_train']:
            if skip_data % n_data == 0:
                image_name = "%s/%s/%s.jpg" % (tmp_folder, tmp_dir, key,)
                image_name = posixpath.normpath(image_name)

                if not os.path.exists(image_name):
                    continue

                cv_image = imread(image_name)
                image_data = resize(cv_image, (width, height), interpolation=INTER_CUBIC)
                image_data = np.array(image_data).astype('uint8')

                if color_scale == GREY_SCALE:
                    image_data = img_to_grey(image_data, settings)

                if process_image:
                    image_data = process_img(image_data, settings)

                x_train.append(image_data)

                for sub_key in data['y_train'][key]:
                    if settings['type'] == "classification":
                        if sub_key == 'steering_angle':
                            y_train_sa.append(_get_classif_action(data['y_train'][key][sub_key], "steer", settings))
                        elif sub_key == 'speed':
                            y_train_speed.append(_get_classif_action(data['y_train'][key][sub_key], "speed", settings))
                    else:
                        if sub_key == 'steering_angle':
                            y_train_sa.append(data['y_train'][key][sub_key])
                        elif sub_key == 'speed':
                            y_train_speed.append(data['y_train'][key][sub_key])

            skip_data += 1

        skip_data = n_data

        for key in data['y_test']:
            if skip_data % n_data == 0:
                image_name = "%s/%s/%s.jpg" % (tmp_folder, tmp_dir, key,)
                image_name = posixpath.normpath(image_name)

                if not os.path.exists(image_name):
                    continue

                cv_image = imread(image_name)
                image_data = resize(cv_image, (width, height), interpolation=INTER_CUBIC)
                image_data = np.array(image_data).astype('uint8')

                if color_scale == GREY_SCALE:
                    image_data = img_to_grey(image_data, settings)

                if process_image:
                    image_data = process_img(image_data, settings)

                x_test.append(image_data)

                # TODO append as tuples?
                for sub_key in data['y_test'][key]:
                    if settings['type'] == "classification":
                        # For Classification
                        if sub_key == 'steering_angle':
                            y_test_sa.append(_get_classif_action(data['y_test'][key][sub_key], "steer", settings))
                        elif sub_key == 'speed':
                            y_test_speed.append(_get_classif_action(data['y_test'][key][sub_key], "speed", settings))
                    else:
                        # For Regression
                        if sub_key == 'steering_angle':
                            y_test_sa.append(data['y_test'][key][sub_key])
                        elif sub_key == 'speed':
                            y_test_speed.append(data['y_test'][key][sub_key])

            skip_data += 1

    x_train = np.array(x_train)
    y_train_speed = np.array(y_train_speed)
    y_train_sa = np.array(y_train_sa)

    x_test = np.array(x_test)
    y_test_speed = np.array(y_test_speed)
    y_test_sa = np.array(y_test_sa)

    if settings['type'] == "classification":
        y_train_sa = np_utils.to_categorical(y_train_sa, settings["action_space"]["steer"]["actions_num_output"] + 1)
        y_train_speed = np_utils.to_categorical(y_train_speed,
                                                settings["action_space"]["speed"]["actions_num_output"] + 1)
        y_test_sa = np_utils.to_categorical(y_test_sa, settings["action_space"]["steer"]["actions_num_output"] + 1)
        y_test_speed = np_utils.to_categorical(y_test_speed,
                                               settings["action_space"]["speed"]["actions_num_output"] + 1)

    if x_train.shape[0] != y_train_sa.shape[0]:
        loginfo('Error (Steering Angle): x_train and y_train have different shapes')
        loginfo(x_train.shape[0], y_train_sa.shape[0])
        raise IOError

    if x_train.shape[0] != y_train_speed.shape[0]:
        loginfo('Error (Speed): x_train and y_train have different shapes')
        loginfo(x_train.shape[0], y_train_speed.shape[0])
        raise IOError

    if x_test.shape[0] != y_test_sa.shape[0]:
        loginfo('Error (Steering Angle): x_test and y_test have different shapes')
        loginfo(x_test.shape[0], y_test_sa.shape[0])
        raise IOError

    if x_test.shape[0] != y_test_speed.shape[0]:
        loginfo('Error (Speed): x_test and y_test have different shapes')
        loginfo(x_test.shape[0], y_test_speed.shape[0])
        raise IOError

    print('All Data read')

    return (x_train, y_train_sa, y_train_speed), (x_test, y_test_sa, y_test_speed)


def _get_classif_action(target_val, action_type, settings):
    target_val_old = target_val

    if target_val > settings["action_space"][action_type]["action_max_limit"]:
        target_val = settings["action_space"][action_type]["action_max_limit"]
    elif target_val < settings["action_space"][action_type]["action_min_limit"]:
        target_val = settings["action_space"][action_type]["action_min_limit"]

    '''
    Example:
    1.
    target_val = 0.12232
    round(target_val, 2) = 0.12
        
    2. 200 / 2 = 100 
        
    0.12 * 100 = 120
    '''

    '''
    Example:
    1.
    target_val = 0.193668857217
    round(target_val, 1) = 0.2

    2. 
    20 / 2 = 10
    
    0.2 * 10 = 2
        
    1.
    target_val = 3
    round(target_val, 3) = 3

    2. 
    20 / 2 = 10
    
    3 * 10 = 3     
    '''

    action = int((round(target_val, settings["action_space"][action_type]["action_decimal_point"]) +
                  settings["action_space"][action_type]["action_max_limit"]) * int(
        settings["action_space"][action_type]["actions_num"] / 2))

    '''
    if action_type == "steer":
        print("Current Target: %s", target_val_old)
        print("Cropped Target: %s", target_val)
        print("Converted Target: %s", action)
    '''
    return action


def _get_data(msg=None):
    # TODO add more values
    return {
        'steering_angle': msg.drive.steering_angle,
        'speed': msg.drive.speed,
    }
