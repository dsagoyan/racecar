'''
Date: 7.7.2017
Author: Dmitry Sagoyan
'''

from __future__ import division

from rl.memory import SequentialMemory
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy, BoltzmannQPolicy, GreedyQPolicy
from rl.agents.dqn import DQNAgent

from keras.optimizers import Adam, RMSprop

from processors.GazeboProcessor import GazeboProcessor
from processors.AtariProcessor import AtariProcessor

import posixpath
import os

import numpy as np


class DQN:
    """DQN wraps implementation of the keras-rl dqn agent"""

    def __init__(self, agent_node, model_classification, comparing_usecases=None):
        print('RL-Agent: DQN')

        self.model_classification = model_classification
        """Model classification"""

        self.env_in = model_classification.env_in
        """Environment description from EnvironmentNode"""

        self.agent_node = agent_node
        """Agent node"""

        memory = SequentialMemory(limit=model_classification.settings["dqn"]["limit"],
                                  window_length=model_classification.settings["dqn"]["window_length"])

        if model_classification.settings["dqn"]["processor"]:
            processor = eval(model_classification.settings["dqn"]["processor"])(
                model_structure=model_classification, settings=model_classification.settings)
        else:
            processor = None

        if model_classification.settings["dqn"]["policy"]["name"] == "GreedyQPolicy":
            policy = GreedyQPolicy()
        elif model_classification.settings["dqn"]["policy"]["name"] == "BoltzmannQPolicy":
            policy = BoltzmannQPolicy()
        elif model_classification.settings["dqn"]["policy"]["name"] == "LinearAnnealedPolicy":
            policy = LinearAnnealedPolicy(EpsGreedyQPolicy(),
                                          attr='eps',
                                          value_max=model_classification.settings["dqn"]["policy"]["eps"]["value_max"],
                                          value_min=model_classification.settings["dqn"]["policy"]["eps"]["value_min"],
                                          value_test=model_classification.settings["dqn"]["policy"]["eps"][
                                              "value_test"],
                                          nb_steps=model_classification.settings["dqn"]["policy"]["eps"]["nb_steps"])
        else:
            policy = GreedyQPolicy()

        if not model_classification.settings["dqn"]["delta_clip"]:
            delta_clip = np.inf
        else:
            delta_clip = model_classification.settings["dqn"]["delta_clip"]

        self.model_agent = DQNAgent(model=model_classification.model,
                                    settings=model_classification.settings,
                                    nb_actions=int(self.env_in.num_actions),
                                    policy=policy,
                                    memory=memory,
                                    enable_double_dqn=model_classification.settings["dqn"]["double_dqn"],
                                    enable_dueling_network=model_classification.settings["dqn"]["dueling_network"],
                                    dueling_type=model_classification.settings["dqn"]["dueling_type"],
                                    processor=processor,
                                    nb_steps_warmup=model_classification.settings["dqn"]["nb_steps_warmup"],
                                    gamma=model_classification.settings["dqn"]["gamma"],
                                    target_model_update=model_classification.settings["dqn"]["target_model_update"],
                                    train_interval=model_classification.settings["dqn"]["train_interval"],
                                    delta_clip=delta_clip)
        """DQN model agent"""

        # Get Learning Rate from comparing settings or general settings
        learning_rate = comparing_usecases["learning_rate"] if comparing_usecases is not None and comparing_usecases[
            "learning_rate"] else \
            model_classification.settings["dqn"]["optimizer"]["learning_rate"]

        # Finally, we configure and compile our agent.
        # You can use every built-in Keras optimizer and even the metrics!
        if model_classification.settings["dqn"]["optimizer"]["name"] == "RMSprop":
            self.model_agent.compile(RMSprop(
                lr=learning_rate,
                rho=model_classification.settings["dqn"]["optimizer"]["rho"],
                epsilon=model_classification.settings["dqn"]["optimizer"]["epsilon"]),
                metrics=['mae'])
        else:
            self.model_agent.compile(Adam(lr=learning_rate), metrics=['mae'])

        if model_classification.settings["dqn"]["weights"]["load"]:
            filepath = posixpath.normpath(model_classification.settings["dqn"]["weights"]["file"])
            if os.path.exists(filepath):
                self.model_agent.load_weights(model_classification.settings["dqn"]["weights"]["file"])
                print("Found DQN Weights and loaded them in the model.")
            else:
                print("It`s impossible to load weights because the file {} doesn`t exist".format(
                    model_classification.settings["dqn"]["weights"]["file"]))

        print("Optimizer: {}, Learning Rate: {}".format(model_classification.settings["dqn"]["optimizer"]["name"],
                                                        learning_rate))

        verboseLogger = model_classification.settings["dqn"]["verbose"]["training_progress"]

        # Start training
        self.model_agent.start_train(env=self.env_in.title, verbose=verboseLogger if verboseLogger else 0)
