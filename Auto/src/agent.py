#!/usr/bin/env python

'''
Date: 7.7.2017
Author: Dmitry Sagoyan
'''

from core.AgentNode import AgentNode

import rospy
import argparse

from utils import settings

if __name__ == '__main__':
    """ Initialize the racecar_agent node. """
    rospy.init_node('racecar_agent')

    parser = argparse.ArgumentParser()
    parser.add_argument('--settings', action="store", default='settings_bgr_20_sim')
    parser.add_argument('--agent', action="store", default='dqn')
    parser.add_argument('--act_msg_type', action="store", default='ActionRacecar')
    parser.add_argument('--sr_msg_type', action="store", default='RLStateRewardRacecar')
    parser.add_argument('--debug', action="store", default='false')
    args = parser.parse_args()

    # Read settings
    settings = settings.read_settings(args.settings)

    # try:

    agent_node = AgentNode(args, settings)
    # except Exception as error:
    #    print(error)

    # Go to the main loop.
    rospy.spin()
