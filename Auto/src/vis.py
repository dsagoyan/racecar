#!/usr/bin/env python

'''
Date: 22.07.2017
Author: Dmitry Sagoyan
'''

from utils import settings, converter
from core.ModelVisualiser import ModelVisualiser
from core.ModelClassification import ModelClassification

if __name__ == '__main__':
    settings_filename = 'settings_bgr_20_sim'

    # Read settings
    settings_data = settings.read_settings(settings_filename)

    # Load base model architecture and load trained weights
    modelClass = ModelClassification(settings=settings_data, vis=True)

    modelVis = ModelVisualiser(modelClass, converter)
