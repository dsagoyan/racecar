import numpy as np
import math


def reward_function_1(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        return env.reward_collision * action_in.episode_step
    elif abs(last_steering_actions_mean) >= 0.5:
        print("Loop Mean: {}".format(last_steering_actions_mean))
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        return env.reward_loop
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor1))
        reward = reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
        return reward


def reward_function_2(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        return env.reward_collision * action_in.episode_step
    elif abs(last_steering_actions_mean) >= 0.5:
        print("Loop Mean: {}".format(last_steering_actions_mean))
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        return env.reward_loop
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor2))
        reward = reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
        return reward


def reward_function_3(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        return -200
    elif abs(last_steering_actions_mean) >= 0.5:
        print("Loop Mean: {}".format(last_steering_actions_mean))
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        return env.reward_loop
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor1))
        reward = round(reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma), 2)
        return reward


def reward_function_4(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        return -200
    elif abs(last_steering_actions_mean) >= 0.5:
        print("Loop Mean: {}".format(last_steering_actions_mean))
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        return env.reward_loop
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor2))
        reward = round(reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma), 2)
        return reward


def reward_function_5(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        return env.reward_collision * action_in.episode_step
    elif abs(last_steering_actions_mean) >= 0.5:
        print("Loop Mean: {}".format(last_steering_actions_mean))
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        return env.reward_loop
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor3))
        reward = reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
        return reward


def reward_function_6(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        return env.reward_collision * action_in.episode_step
    elif abs(last_steering_actions_mean) >= 0.5:
        print("Loop Mean: {}".format(last_steering_actions_mean))
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        return env.reward_loop
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor4))
        reward = reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
        return reward


# slope { 2: parallel , 1: only one side is parallel, 0- no parallel found
def reward_function_7(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        reward_return = env.reward_collision * action_in.episode_step
    elif abs(last_steering_actions_mean) >= 0.5:
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        print("Loop Mean: {}".format(last_steering_actions_mean))
        reward_return = env.reward_loop
    elif lm_data is not None and lm_data.slope == 2:
        reward_return = env.reward_bonus_val + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
    elif lm_data is not None and lm_data.slope == 1:
        reward_return = env.reward_val + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor2))
        reward_return = reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma)

    return reward_return


# slope { 2: parallel , 1: only one side is parallel, 0- no parallel found
def reward_function_8(env, action_in, lm_data=None):
    last_steering_actions_mean = np.mean(env.last_steering_actions)

    if env.terminal():
        reward_return = env.reward_collision * action_in.episode_step
    elif abs(last_steering_actions_mean) >= 0.5:
        # Punishments for looping. If the car tries to make loops and collect rewards, it gets negative rewards
        print("Loop Mean: {}".format(last_steering_actions_mean))
        reward_return = env.reward_loop
    elif lm_data is not None and lm_data.slope == 2:
        reward_return = env.reward_bonus_val + math.pow(action_in.reward_multiplier, env.reward_val_gamma)
    else:
        reward = (env.reward_val - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor4))
        reward_return = reward + math.pow(action_in.reward_multiplier, env.reward_val_gamma)

    return reward_return


# slope { 2: parallel , 1: only one side is parallel, 0- no parallel found
def reward_function_9(env, action_in, lm_data=None):
    env.is_looping()

    if env.terminal():
        reward_return = env.reward_collision_simple
    elif lm_data is not None and lm_data.slope == 2:
        reward_return = env.reward_val_simple
    else:
        reward_return = (
            env.reward_val_simple - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor_simple))

    return reward_return


def reward_function_10(env, action_in, lm_data=None):
    if env.terminal():
        reward_return = env.reward_collision_simple
    else:
        reward_return = (
            env.reward_val_simple - (abs(action_in.steer_action) * env.reward_val_turn_decrease_factor_simple))

    return round(reward_return, 2)
