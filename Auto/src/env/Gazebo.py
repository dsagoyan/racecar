#!/usr/bin/env python

'''
Date: 22.04.2017
Author: Dmitry Sagoyan
'''

from racecar_msgs.msg import RLStateRewardRacecar
from racecar_msgs.msg import ActionRacecar

from ackermann_msgs.msg import AckermannDriveStamped

from racecar_msgs.msg import CompressedImage
from sensor_msgs.msg import LaserScan

from gazebo_msgs.srv import SetModelState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates

from geometry_msgs.msg import Pose, Twist

from std_srvs.srv import Empty as EmptySrv

from Env import Env

from racecar_msgs.msg import LaserScanSectorsDistance

from collections import deque

import time
import rospy
import tf

from Rewards import *


class Gazebo(Env):
    def __init__(self, parent, drive_control):
        self.parent = parent

        # Can be useful if an action has
        # a very small effect on the environment.
        self.delay_after_action = None if not self.parent.settings["dqn"]["delay_after_action"] else \
            self.parent.settings["dqn"]["delay_after_action"]

        self.close_distance_front = 0.3
        """Defines minimal distance to the objects"""

        self.reward_collision = -4.0
        self.reward_loop = -2.0

        self.reward_val = 5.0
        self.reward_bonus_val = 8.0

        # Turn left and right decrease factor
        self.reward_val_turn_decrease_factor1 = 1
        self.reward_val_turn_decrease_factor2 = 2
        self.reward_val_turn_decrease_factor3 = 3
        self.reward_val_turn_decrease_factor4 = 4

        # Alternative definition of the reward collision, positive and turn left and right decrease factor
        self.reward_collision_simple = -1.0
        self.reward_val_simple = 1.0
        self.reward_val_turn_decrease_factor_simple = 0.5

        self.first_state_sent = False

        self.reward_val_gamma = 1.2

        self.last_robot_poses = None

        self.envname = parent.arguments.envname

        self.debug = parent.arguments.debug

        self.drive_control = drive_control

        self.robot_model_name = self.parent.settings["env_node"]["robot_model_name"]

        self.world_points = self.get_world_points()

        self.world_bounds = self.get_world_bounds()

        self.setup_publishers()

        self.setup_subscribers()

        self.setup_services()

        # Set default reward function
        self.reward_function = reward_function_10

        # Make small pause and only then reset the environment
        time.sleep(0.3)

    def _get_sensor_data(self):
        """Get different sensor data in time t"""

        # LocationMonitor
        if self.parent.settings["dqn"]["features"]["lm_manager"]:
            try:
                lm_data = rospy.wait_for_message('/lm_obstacle_distance',
                                                 LaserScanSectorsDistance,
                                                 timeout=5)
            except:
                rospy.logerr('Error on getting Scan')
                lm_data = None
                pass
        else:
            lm_data = None

        # Lidar
        try:
            laser_data = rospy.wait_for_message('/scan',
                                                LaserScan,
                                                timeout=5)
        except:
            rospy.logerr('Error on getting Scan')
            laser_data = None
            pass

        # Image
        try:
            image_data = rospy.wait_for_message('/camera/zed/rgb/image_rect_color/compressed',
                                                CompressedImage,
                                                timeout=5)
        except:
            rospy.logerr('Error on getting Image')
            return self._get_sensor_data()

        return laser_data, image_data, lm_data

    def get_world_points(self):
        world = self.parent.settings["env_node"]["world"]
        world_points = self.parent.settings["env_node"]["worlds"][world]["points"]

        points = []

        for point in world_points:
            point = self.parent.settings["env_node"]["worlds"][world]["points"][point]

            quaternion = tf.transformations.quaternion_from_euler(point["orientation"]["roll"],
                                                                  point["orientation"]["pitch"],
                                                                  point["orientation"]["yaw"])

            point["orientation"]["x"] = quaternion[0]
            point["orientation"]["y"] = quaternion[1]
            point["orientation"]["z"] = quaternion[2]
            point["orientation"]["w"] = quaternion[3]

            points.append(point)

        return points

    def get_world_bounds(self):
        world = self.parent.settings["env_node"]["world"]
        world_bounds = []

        if self.parent.settings["env_node"]["collision"]["reset_model_if_out_world_bounds"]:
            world_bounds = self.parent.settings["env_node"]["worlds"][world]["bounds"]

        return world_bounds

    def _select_reward_function(self, reward_function='reward_function_10'):
        return {
            'reward_function_1': reward_function_1,
            'reward_function_2': reward_function_2,
            'reward_function_3': reward_function_3,
            'reward_function_4': reward_function_4,
            'reward_function_5': reward_function_5,
            'reward_function_6': reward_function_6,
            'reward_function_7': reward_function_7,
            'reward_function_8': reward_function_8,
            'reward_function_9': reward_function_9,
            'reward_function_10': reward_function_10,
        }.get(reward_function, reward_function_10)

    def set_reward_function(self, usecase):
        self.reward_function = self._select_reward_function(usecase["reward_function"])

    def setup_services(self):
        self.g_pause = rospy.ServiceProxy("/gazebo/pause_physics", EmptySrv)
        self.g_unpause = rospy.ServiceProxy("/gazebo/unpause_physics", EmptySrv)
        self.g_set_state = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)

    def setup_subscribers(self):
        rospy.Subscriber("/gazebo/model_states",
                         ModelStates,
                         self.process_get_model_state,
                         queue_size=1)

    def setup_publishers(self):
        self.pub_nav = rospy.Publisher("/vesc/ackermann_cmd_mux/input/teleop",
                                       AckermannDriveStamped,
                                       queue_size=1)

    def get_action_message_type(self, retClass=True):
        if (retClass):
            return ActionRacecar
        else:
            return ActionRacecar()

    def get_sr_message_type(self, retClass=True):
        if retClass:
            return RLStateRewardRacecar
        else:
            return RLStateRewardRacecar()

    def get_env_name(self):
        return self.envname

    def get_num_actions(self):
        """Actions Qty"""
        return self.num_actions

    def process_action(self, action_in):
        """Process the generated action from the RL-Agent"""
        sr = self.get_sr_message_type(False)

        if self.parent.settings["dqn"]["pause_during_training"]:
            self.unpause()

        # Process action from the agent, affecting the environment.
        # Don't forget, apply function includes subfunction which pauses the physics
        observation, reward, terminal = self.apply(action_in)

        sr.state = observation
        sr.terminal = terminal
        sr.reward = reward

        if self.parent.arguments.debug:
            # Publish the state-reward message
            print "\nMake Action: {} and get Reward: {}".format(action_in.steer_action, sr.reward)

        self.parent.pub_env_sr.publish(sr)

    def process_get_model_state(self, data):
        if not self.first_state_sent:
            return

        # Get curret robot position
        robot_pos = data.pose[data.name.index(self.robot_model_name)].position

        if self.parent.settings["env_node"]["collision"]["reset_model_standing_on_place"]:
            # Add last robot pos (x,y)
            if self.last_robot_poses is not None and not self.is_standing_on_place:
                robot_pos_xy = round(robot_pos.x + robot_pos.y, 5)
                self.last_robot_poses.append(robot_pos_xy)

                if round(np.mean(self.last_robot_poses), 5) == robot_pos_xy and len(
                        self.last_robot_poses) >= (self.last_robot_poses.maxlen - 1):
                    self.is_standing_on_place = True
                    print("Standing on place")
                    print("Collected: {}".format(len(self.last_robot_poses)))
                    print("Mean: {}".format(np.mean(self.last_robot_poses)))
                    print("Mean Round: {}".format(round(np.mean(self.last_robot_poses), 5)))
                    print("Current Pos Round: {}".format(robot_pos_xy))

        if self.parent.settings["env_node"]["collision"]["reset_model_if_out_world_bounds"]:
            # not (inbouds)
            if not (self.world_bounds['x1'] < robot_pos.x and \
                                self.world_bounds['y1'] < robot_pos.y and \
                                self.world_bounds['x2'] > robot_pos.x and \
                                self.world_bounds['y2'] < robot_pos.x and \
                                self.world_bounds['x3'] > robot_pos.x and \
                                self.world_bounds['y3'] > robot_pos.y and \
                                self.world_bounds['x4'] < robot_pos.x and \
                                self.world_bounds['y4'] > robot_pos.y):
                self.is_out_of_bounds = True
            else:
                self.is_out_of_bounds = False

    def _process_laser_scan(self, data):
        if data is not None:
            collisions = filter(lambda x: not math.isinf(x) and x <= self.close_distance_front, data.ranges)

            if len(collisions) > 0:
                self.is_collision = 1
            else:
                self.is_collision = 0
        else:
            self.is_collision = 0

    def _encode_distance(self, val, close_distance):
        if val <= close_distance or math.isnan(val):
            val = 0
        elif val > close_distance:
            val = 1

        return val

    def reset(self):
        """Reset states, coordinates, velocity, angle of robot"""
        self.first_state_sent = False

        # There are no collisions
        self.is_collision = 0

        # Robot is inside the track
        self.is_out_of_bounds = False

        # If standing for long time on the same place, then will be true
        self.is_standing_on_place = False

        # If standing for long time on the same place, then will be true
        self.is_steering_in_loop = False

        self.last_steering_actions = deque(np.zeros(1, dtype='float16'), maxlen=50)

        # Collect latest positions of the robot
        self.last_robot_poses = deque(np.zeros(1, dtype='float16'), maxlen=500)

        # Reset model speed to 0
        if self.parent.settings["env_node"]["collision"]["reset_model_speed"]:
            self.reset_model_speed()

        # Reset model position
        if self.parent.settings["env_node"]["collision"]["reset_model_pos"]:
            self.reset_model_pos()

        # This sleep is very important because when process_episode_info
        # fires reset function (this one) the model has no time to change
        # position, so that the terminal is true insteed of new state terminal=false,
        # and the episode ends. Maybe its needed to find callback for the service and
        # when it will be fired, start other steps from process_episode_info
        time.sleep(0.3)

    def pause(self):
        """Pause the Physics-Engine"""
        rospy.wait_for_service("/gazebo/pause_physics")
        try:
            self.g_pause()
        except Exception, e:
            rospy.logerr('Error on calling service: %s', str(e))

    def unpause(self):
        """Unpause the Physics-Engine"""
        rospy.wait_for_service("/gazebo/unpause_physics")
        try:
            self.g_unpause()
        except Exception, e:
            rospy.logerr('Error on calling service: %s', str(e))

    def reset_model_pos(self):
        """
        Reset of the position of the car model.
        It can be random position or first position from the config list
        """
        if self.parent.settings["env_node"]["collision"]["reset_model_pos_random"]:
            point_selected = np.random.randint(0, len(self.world_points))
        else:
            point_selected = 0

        pose = Pose()
        pose.position.x = self.world_points[point_selected]["position"]["x"]
        pose.position.y = self.world_points[point_selected]["position"]["y"]
        pose.position.z = self.world_points[point_selected]["position"]["z"]
        pose.orientation.x = self.world_points[point_selected]["orientation"]["x"]
        pose.orientation.y = self.world_points[point_selected]["orientation"]["y"]
        pose.orientation.z = self.world_points[point_selected]["orientation"]["z"]
        pose.orientation.w = self.world_points[point_selected]["orientation"]["w"]

        state = ModelState()
        state.model_name = self.parent.settings["env_node"]["robot_model_name"]
        state.pose = pose

        try:
            self.g_set_state(state)
        except Exception, e:
            rospy.logerr('Error on calling service: %s', str(e))

    def reset_model_speed(self):
        a_d_s = self.drive_control.update_driving(_steering=0,
                                                  _accelerator=0,
                                                  _brake=0,
                                                  _speed=0)
        self.pub_nav.publish(a_d_s)

    def apply(self, action_in):
        # Add a new action in the last steering actions list
        self.last_steering_actions.append(action_in.steer_action)

        acc_command = -1.0
        brake_command = 1.0

        if not self.parent.settings["env_node"]["manual_control"]:
            a_d_s = self.drive_control.update_driving(_steering=action_in.steer_action,
                                                      _accelerator=acc_command,
                                                      _brake=brake_command)

            # The model drives
            self.pub_nav.publish(a_d_s)

        # Small timeout `delay_after_action` before the model drives and we get then reward
        if self.delay_after_action is not None:
            time.sleep(self.delay_after_action)

        return self.get_observation_reward_terminal(action_in=action_in, firstState=False)

    def get_observation_reward_terminal(self, action_in=None, firstState=True):
        """Get sensor data and return observation, reward and terminal states
        note: it is needed to unpause world to get the current sensor data

        :param action_in: includes action from the RL-Agent
        :param firstState: if state is first, then the process_action was not called
        :return: returns image, reward, terminal states
        """
        if firstState and self.parent.settings["dqn"]["pause_during_training"]:
            self.unpause()

        # Get Sensor Data
        laser_data, image_data, lm_data = self._get_sensor_data()

        # Pause physics
        if self.parent.settings["dqn"]["pause_during_training"]:
            self.pause()

        # process laser data to know if the terminal state is reached. Check terminal()[false,true]
        self._process_laser_scan(laser_data)

        return image_data, self.reward(action_in, lm_data), self.terminal()

    def terminal(self):
        return self.is_collision == 1 or \
               self.is_out_of_bounds or \
               self.is_standing_on_place or \
               self.is_steering_in_loop

    def is_episodic(self):
        return True

    def get_minmax_features(self):
        return [[0], [255]]

    def reward(self, action_in=None, lm_data=None):
        if action_in is not None:
            return self.reward_function(self, action_in, lm_data)
        else:
            return self.reward_val

    def is_looping(self):
        """Check if the model trying to loop during a steer and marks actions as terminated"""
        if abs(np.mean(self.last_steering_actions)) >= 0.5:
            print("Made loop... Terminate")
            self.is_steering_in_loop = True
