#!/usr/bin/env python

'''
Date: 23.04.2017
Author: Dmitry Sagoyan
'''

import gym
import numpy as np

from racecar_msgs.msg import RLStateReward
from racecar_msgs.msg import RLStateRewardRacecar

from racecar_msgs.msg import Action

from processors.AtariProcessor import AtariProcessor

from Env import Env


class Openai(Env):
    """
    https://github.com/openai/gym/wiki/CartPole-v0
    https://github.com/openai/gym/wiki/BreakoutDeterministic-v3
    """

    env = None

    def __init__(self, parent):
        self.parent = parent

        # Reward is 1 for every step taken, including the termination step
        self.reward_collision = 1
        self.reward_val = 1

        self.envname = self.parent.arguments.envname
        self.render = self.parent.arguments.render
        self.debug = self.parent.arguments.debug

        self.processor = None

        self.env = gym.make(self.envname)

        np.random.seed(123)
        self.env.seed(123)

        self.reset()

    def get_action_message_type(self, retClass=True):
        if (retClass):
            return Action
        else:
            return Action()

    def get_sr_message_type(self, retClass=True):
        if (retClass):
            return eval(self.parent.arguments.sr_msg_type)
        else:
            return eval(self.parent.arguments.sr_msg_type)()

    def get_env_name(self):
        return self.envname

    # Actions Qty.
    def get_num_actions(self):
        return self.env.action_space.n

    # Reset states
    def reset(self):
        self.terminal_val = False

        # Prepare states
        self.state_val = self.env.reset()

    # Get current state object
    def sensation(self):
        if self.render:
            self.env.render()

        return self.state_val

    def apply(self, action_in):

        self.state_val, self.reward_val, self.terminal_val, _ = self.env.step(action_in.action)

        return self.get_observation_reward_terminal(action_in, firstState=False)

    def get_observation_reward_terminal(self, action_in=None, firstState=True):
        """Get sensor data and return observation, reward and terminal states
        note: it is needed to unpause world to get the current sensor data

        :param action_in: includes action from the RL-Agent
        :param firstState: if state is first, then the process_action was not called
        :return: returns observation, reward, terminal states
        """

        return self.state_val, self.reward_val, self.terminal_val

    def reward(self):
        return self.reward_val

    # Terminal? Done?
    def terminal(self):
        return self.terminal_val

    def is_episodic(self):
        return True

    def get_minmax_features(self):
        for i in self.env.observation_space.high:
            self.max_feat.append(i)

        for i in self.env.observation_space.low:
            self.min_feat.append(i)

        return [self.min_feat, self.max_feat]

    def process_action(self, action_in):
        """Process the generated action from the RL-Agent"""
        sr = self.get_sr_message_type(False)

        # Process action from the agent, affecting the environment.
        # Don't forget, apply function includes subfunction which pauses the physics
        observation, reward, terminal = self.apply(action_in)

        sr.state = observation
        sr.terminal = terminal
        sr.reward = reward

        if self.processor is not None:
            sr.state = self.processor.process_observation(sr.state)

        if self.parent.arguments.debug:
            # Publish the state-reward message
            print "\nMake Action: {} at State: {} => Reward: {}".format(action_in.action, sr.state[0],
                                                                        sr.reward)
        self.parent.pub_env_sr.publish(sr)
