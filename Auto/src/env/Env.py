#!/usr/bin/env python

'''
Date: 23.04.2017
Author: Dmitry Sagoyan
'''


class Env:
    """Env is abstract environment class"""

    num_actions = 21
    """Number of actions"""

    def get_action_message_type(self, retClass=True):
        """Gives either Action class or object.

                # Argument
                    retClass: (Boolean): If true, then Action class will be sent, otherwise object

                # Returns
                    Action class or object
                """
        raise NotImplementedError()

    def get_sr_message_type(self, retClass=True):
        """Gives either StateReward class or object.

        # Argument
            retClass: (Boolean): If true, then StateReward Class will be sent, otherwise object

        # Returns
            StateReward class or object
        """
        raise NotImplementedError()

    def get_env_name(self):
        """Get name of environment"""
        raise NotImplementedError()

    def get_num_actions(self):
        """Get quantity of the actions of environment"""
        raise NotImplementedError()

    def reset(self):
        """Reset environment to the start state"""
        raise NotImplementedError()

    def apply(self, action_in):
        """Applies action to the environment"""
        raise NotImplementedError()

    def reward(self):
        """Creates reward"""
        raise NotImplementedError()

    def terminal(self):
        """Checks terminal conditions"""
        raise NotImplementedError()

    def is_episodic(self):
        """Is env episodic?"""
        raise NotImplementedError()

    def set_reward_function(self):
        """Set Reward Function"""
        raise NotImplementedError()

    def get_minmax_features(self):
        """Set MinMaxFeatures to know different between features"""
        raise NotImplementedError()

    def process_action(self, action_in):
        """Callback function of /rl_agent/rl_action. Receives action from rl-agent"""
        raise NotImplementedError()

    def get_observation_reward_terminal(self, action_in=None, firstState=False):
        raise NotImplementedError()