#!/usr/bin/env python
import json
import os
import posixpath

from keras import backend as k
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
from keras.preprocessing.image import ImageDataGenerator

from rospy import loginfo

from core.Model import SupervisedModel

from utils import converter
from utils import file_writer
from utils import settings


def _start_training(
        supervised_model,
        batch_size,
        epochs,
        x_train,
        y_train,
        x_test,
        y_test,
        log_nn_structure=True,
        log_performance=True,
        log_tensor_board=False,
        verbose=0,
        model_save=False,
        model_save_weights_only=False):
    if k.image_dim_ordering() == 'th':
        # Theano-Backend
        x_train = x_train.reshape(x_train.shape[0], supervised_model.img_depth, supervised_model.settings["img_height"],
                                  supervised_model.settings["img_width"])
        x_test = x_test.reshape(x_test.shape[0], supervised_model.img_depth, supervised_model.settings["img_height"],
                                supervised_model.settings["img_width"])
    else:
        # Tensorflow-Backend
        x_train = x_train.reshape(x_train.shape[0], supervised_model.settings["img_height"],
                                  supervised_model.settings["img_width"],
                                  supervised_model.img_depth)
        x_test = x_test.reshape(x_test.shape[0], supervised_model.settings["img_height"],
                                supervised_model.settings["img_width"],
                                supervised_model.img_depth)

    x_train = x_train.astype('float16')
    x_test = x_test.astype('float16')

    x_train /= 255
    x_test /= 255

    callbacks_list = []

    tmp_dir = os.path.dirname(__file__)

    if model_save:
        # checkpoint
        filepath = "trained_data/%s-best.hdf5" % (supervised_model.settings["model_name"],)
        filepath = os.path.join(tmp_dir, '..', filepath)
        filepath = posixpath.normpath(filepath)
        checkpoint = ModelCheckpoint(filepath=filepath,
                                     monitor='val_acc',
                                     verbose=verbose,
                                     save_best_only=True,
                                     save_weights_only=model_save_weights_only,
                                     mode='max')
        callbacks_list.append(checkpoint)

    if log_tensor_board:
        filepath = "graphs/%s" % (supervised_model.settings["model_name"],)
        filepath = os.path.join(tmp_dir, '..', filepath)
        filepath = posixpath.normpath(filepath)
        if not os.path.exists(filepath):
            os.makedirs(filepath)
        tb_call_back = TensorBoard(log_dir=filepath, histogram_freq=0, write_graph=True, write_images=True)
        callbacks_list.append(tb_call_back)

    if supervised_model.settings["supervised_learning"]["data_augmentation"]["status"] != True:
        print('Not using data augmentation.')

        history = supervised_model.model.fit(x_train,
                                             y_train,
                                             batch_size=batch_size,
                                             epochs=epochs,
                                             callbacks=callbacks_list,
                                             verbose=verbose,
                                             validation_data=(x_test, y_test))

    elif supervised_model.settings["supervised_learning"]["data_augmentation"]["status"] == True:

        print('Using real-time data augmentation.')

        data_gen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=10,  # randomly rotate data in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift data horizontally (fraction of total width)
            height_shift_range=0.1,  # randomly shift data vertically (fraction of total height)
            shear_range=0.2,
            zoom_range=0.1,
            channel_shift_range=0.1,
            fill_mode='nearest',
        )

        # Compute quantities required for featurewise normalization
        # (std, mean, and principal components if ZCA whitening is applied).
        data_gen.fit(x_train)

        # model.model = model.load_model()

        if supervised_model.settings["supervised_learning"]["data_augmentation"]["save_to_dir"] != False:
            if len(supervised_model.settings["supervised_learning"]["data_augmentation"]["save_to_dir"]) > 0 and \
                            supervised_model.settings["supervised_learning"]["data_augmentation"][
                                "save_to_dir"] is not None:
                save_to_dir = posixpath.normpath(
                    supervised_model.settings["supervised_learning"]["data_augmentation"]["save_to_dir"])
        else:
            save_to_dir = None

        data_get_flow = data_gen.flow(x_train,
                                      y_train,
                                      batch_size=batch_size,
                                      save_to_dir=save_to_dir,
                                      save_prefix=supervised_model.settings["supervised_learning"]["data_augmentation"][
                                          "save_prefix"],
                                      save_format=supervised_model.settings["supervised_learning"]["data_augmentation"][
                                          "save_format"]);

        history = supervised_model.model.fit_generator(data_get_flow,
                                                       steps_per_epoch=x_train.shape[0] / batch_size * 50,
                                                       epochs=epochs,
                                                       validation_data=(x_test, y_test),
                                                       callbacks=callbacks_list,
                                                       verbose=verbose)
    if log_nn_structure:
        file_writer.write_nn_structure(supervised_model.model)

    if log_performance:
        file_writer.write_log_text(supervised_model.settings["model_name"], history)
        file_writer.write_log_graphs(supervised_model.settings["model_name"], history)


def train_racecar():
    # Settings configuration

    settings_filename = 'settings_bgr_20_sim'
    # settings_filename = 'settings_bgr_regression_sim'
    # settings_filename = 'settings_bgr_200_100'
    # settings_filename = 'settings_bgr_200'

    # Read settings
    settings_data = settings.read_settings(settings_filename)

    # If True, saves model and weights
    model_save = settings_data["supervised_learning"]["model_save"]

    # If True, saves weights of the model seperate
    model_save_weights_only = settings_data["supervised_learning"]["model_save_weights_only"]

    batch_size = settings_data["supervised_learning"]["hyperparameters"]["batch_size"]
    epochs = settings_data["supervised_learning"]["hyperparameters"]["epochs"]
    verbose = settings_data["supervised_learning"]["verbose"]  # show log while training

    # If True, convert bags into image and json data
    convert_from_bags = settings_data["supervised_learning"]["converter"]["convert_from_bags"]

    # If True, takes dataset from already converted bags
    load_from_data = settings_data["supervised_learning"]["converter"]["load_from_data"]

    # Take each n-th data from bag
    n_data_from_bags = settings_data["supervised_learning"]["converter"]["n_data_from_bags"]

    # Take each n-th data from files
    n_data_from_data = settings_data["supervised_learning"]["converter"]["n_data_from_data"]

    # Empty list will use all subfolders
    data_sub_folders = settings_data["supervised_learning"]["converter"]["data_sub_folders"]

    # Empty list will use all bags
    data_bags = settings_data["supervised_learning"]["converter"]["data_bags"]

    # Empty list will use all bags
    bags_to_convert = settings_data["supervised_learning"]["converter"]["bags_to_convert"]

    # target directory inside training/data/
    convert_to_dir = settings_data["supervised_learning"]["converter"]["convert_to_dir"]

    log_performance = settings_data["supervised_learning"]["log"]["performance"]
    log_nn_structure = settings_data["supervised_learning"]["log"]["nn_structure"]  # True was
    log_tensor_board = settings_data["supervised_learning"]["log"]["tensor_board"]  # log file can become quite large

    # Convert and create data
    if convert_from_bags:
        # Convert from rosbags to data
        converter.convert_rosbags_to_data(settings_data, bags=bags_to_convert, folder_dir=convert_to_dir,
                                          n_data=n_data_from_bags)

    # Create Supervised-Model
    supervised_model = SupervisedModel(settings=settings_data, training=True)

    loginfo('Loading data for %s.' % (settings_filename))

    if load_from_data:
        # Get data from data folder
        (x_train, y_train_sa, y_train_speed), (x_test, y_test_sa, y_test_speed) = converter.return_data(
            settings=settings_data,
            folders=data_sub_folders,
            color_scale=supervised_model.settings["color_scale"],
            height=supervised_model.settings["img_height"],
            width=supervised_model.settings["img_width"],
            process_image=supervised_model.settings["process_image"],
            n_data=n_data_from_data)
    else:
        # Get data from rosbags directly
        (x_train, y_train_sa, y_train_speed), (x_test, y_test_sa, y_test_speed) = converter.return_rosbags(
            settings=settings_data,
            bags=data_bags,
            color_scale=supervised_model.settings["color_scale"],
            height=supervised_model.settings["img_height"],
            width=supervised_model.settings["img_width"],
            process_image=supervised_model.settings["process_image"],
            n_data=n_data_from_bags)

    print('Finished converting\n')

    if settings_data["supervised_learning"]["train_after_converting"] == False:
        exit(0)

    print('Start training...\n')

    # START TRAINING AND EVALUATION
    _start_training(supervised_model=supervised_model,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=verbose,
                    log_nn_structure=log_nn_structure,
                    log_performance=log_performance,
                    log_tensor_board=log_tensor_board,
                    x_train=x_train,
                    y_train=y_train_sa,
                    x_test=x_test,
                    y_test=y_test_sa,
                    model_save=model_save,
                    model_save_weights_only=model_save_weights_only)

    print('Training successful.')


if __name__ == '__main__':
    train_racecar()
