#!/usr/bin/env python
from utils import settings
from core.Autonomous import Autonomous
from core.Model import SupervisedModel

if __name__ == '__main__':
    settings_filename = 'settings_bgr_20_sim'
    # settings_filename = 'settings_bgr_regression_sim'
    # settings_filename = 'settings_bgr_200_100'
    # settings_filename = 'settings_bgr_200'

    # Read settings
    settings_data = settings.read_settings(settings_filename)
    supervised_model = SupervisedModel(settings=settings_data, training=False)
    Autonomous(supervised_model).start()
