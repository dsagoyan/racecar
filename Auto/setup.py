#!/usr/bin/env python
import os

if __name__ == '__main__':
    root_dir = os.path.dirname(__file__)

    # creates folders for training data
    training_dir = "%s/training_data" % (root_dir,)
    if not os.path.exists(training_dir):
        os.makedirs(training_dir)
    rosbags = "%s/rosbags" % (training_dir,)
    if not os.path.exists(rosbags):
        os.makedirs(rosbags)
    data = "%s/data" % (training_dir,)
    if not os.path.exists(data):
        os.makedirs(data)

    # creates folder for trained data
    trained_data = "%s/trained_data" % (root_dir,)
    if not os.path.exists(trained_data):
        os.makedirs(trained_data)

    # creates folder for graphs
    graphs = "%s/graphs" % (root_dir,)
    if not os.path.exists(graphs):
        os.makedirs(graphs)

    # creates folder for logs
    logs = "%s/logs" % (root_dir,)
    if not os.path.exists(logs):
        os.makedirs(logs)
